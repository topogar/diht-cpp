/* Дана строка длины n. Найти количество ее различных подстрок. Используйте суффиксный массив.

Построение суффиксного массива выполняйте за O(n log n). Вычисление количества различных подстрок выполняйте за O(n). */

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

const int alphavetPower = 27;
const char firstSymbol = '`';

class SuffixArray {
private:
    std::string sourse;
    std::vector<int> classes;
    std::vector<int> suffixArray;
    int size;
    int numberClasses;
    void firstIteration();
    void makeSuffixArray();
public:
    SuffixArray(const std::string &str);
    std::vector<int> giveSuffixArray();
    std:: string giveString();
};

SuffixArray::SuffixArray(const std::string &str) : sourse(str + '`')
        , classes(str.size() + 1), suffixArray(str.size() + 1, 0), size(str.size() + 1) {
    firstIteration();
    makeSuffixArray();
}

std::string SuffixArray::giveString() {
    return sourse.substr(0, sourse.size() - 1);
}

std::vector<int> SuffixArray::giveSuffixArray() {
    return suffixArray;
}

void SuffixArray::firstIteration() {
    std::vector<int> count(alphavetPower, 0);
    for (char symbol : sourse) {
        count[symbol - firstSymbol]++;
    }
    for (int i = 1; i < alphavetPower; i++) {
        count[i] += count[i - 1];
    }
    for (int i = 0; i < size; i++) {
        suffixArray[count[sourse[i] - firstSymbol] - 1] = i;
        count[sourse[i] - firstSymbol]--;
    }
    numberClasses = 0;
    classes[suffixArray[0]] = 0;
    for (int i = 1; i < size; i++) {
        if (sourse[suffixArray[i]] != sourse[suffixArray[i - 1]]) {
            numberClasses++;
        }
        classes[suffixArray[i]] = numberClasses;
    }
    numberClasses++;

}

void SuffixArray::makeSuffixArray() {

    std::vector<int> newSuffixArray(size, 0);
    for (int length = 1; length < size;) {
        for (int i = 0; i < size; i++) {
            newSuffixArray[i] = suffixArray[i] - length;
            if (newSuffixArray[i] < 0) {
                newSuffixArray[i] += size;
            }
        }
        std::vector<int> count(numberClasses, 0);
        for (int i = 0; i < size; i++) {
            count[classes[i]]++;
        }

        for (int i = 1; i < numberClasses; i++) {
            count[i] += count[i - 1];
        }
        /*for (int i = size - 1; i > 0; i--) {
            count[i] = count[i - 1];
        }
        count[0] = 0;
        for (int i = 0; i < size; i++) {
            suffixArray[count[classes[newSuffixArray[i]]]] = newSuffixArray[i];
            count[classes[newSuffixArray[i]]]++;
        }*/
        for (int i = size - 1; i >= 0; i--)
            suffixArray[--count[classes[newSuffixArray[i]]]] = newSuffixArray[i];
        std::vector<int> newClasses(size, 0);
        numberClasses = 0;
        newClasses[suffixArray[0]] = 0;
        for (int i = 1; i < size; i++) {
            int middle = (suffixArray[i] + length) % size;
            int prevMiddle = (suffixArray[i - 1] + length) % size;
            if (classes[suffixArray[i]] != classes[suffixArray[i - 1]] ||
                    classes[middle] != classes[prevMiddle]) {
                numberClasses++;
            }
            newClasses[suffixArray[i]] = numberClasses;
        }
        for (int i = 0; i < size; i++) {
            classes[i] = newClasses[i];
        }
        numberClasses++;
        length = length + length;
    }
}

int differentSubstrings(const std::vector<int> &suffixArray, const std::string &str) {
    int size = str.size();
    std::vector<int> suffixPosition(size, 0);
    for (int i = 0; i < size; i++) {
        suffixPosition[suffixArray[i]] = i;
    }
    std::vector<int> lcp(size, 0);
    int k = 0;
    for (int i = 0; i < size; i++) {
        k = std::max(k - 1, 0);
        if (suffixPosition[i] == size - 1) {
            lcp[size - 1] = 0;
            k = 0;
        } else {
            int nextSuffix = suffixArray[suffixPosition[i] + 1];
            while ((i + k < size) && (nextSuffix + k < size) && (str[i + k] == str[nextSuffix + k])) {
                k++;
            }
            lcp[suffixPosition[i]] = k;
        }
        //std::cout << k;
    }
    /*for (int i = 0; i < size; i++) {
        std::cout << lcp[i];
    }*/
    int different = 0;
    for (int i = 1; i < size; i++) {
        different += size - 1 - suffixArray[i];
    }
    for (int i = 1; i < size; i++) {
        different -= lcp[i];
    }
    return different;
}

int main() {
    std::string str;
    std::cin >> str;
    std::vector<int> suf(str.size() + 1, 0);
    suf = SuffixArray(str).giveSuffixArray();
//    for (int i = 1; i < str.size() + 1; i++) {
//        std::cout << suf[i] << " ";
//    }
    str = str + '`';
    std::cout << differentSubstrings(suf, str);
    return 0;
}
