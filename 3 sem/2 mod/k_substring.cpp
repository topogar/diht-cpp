/* Заданы две строки s, t и целое число k. Рассмотрим множество всех таких непустых строк, которые встречаются как подстроки в s и t одновременно. Найдите k-ую в лексикографическом порядке строку из этого множества. 
Полезная статья про сравнение указателей: https://stackoverflow.com/questions/9086372/how-to-compare-pointers. */

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

const int alphavetPower = 256;
const char firstSymbol = '`';

class SuffixArray {
private:
    std::string sourse;
    std::vector<long long> classes;
    std::vector<long long> suffixArray;
    long long size;
    int numberClasses;
    void firstIteration();
    void makeSuffixArray();
public:
    SuffixArray(const std::string &str);
    std::vector<long long> giveSuffixArray();
    std::string giveString();
};

SuffixArray::SuffixArray(const std::string &str) : sourse(str + '$')
        , classes(str.size() + 1), suffixArray(str.size() + 1, 0), size(str.size() + 1) {
    firstIteration();
    makeSuffixArray();
}

std::string SuffixArray::giveString() {
    return sourse.substr(0, sourse.size() - 1);
}

std::vector<long long> SuffixArray::giveSuffixArray() {
    return suffixArray;
}

void SuffixArray::firstIteration() {
    std::vector<long long> count(alphavetPower, 0);
    for (char symbol : sourse) {
        count[symbol]++;
    }
    for (long long i = 1; i < alphavetPower; i++) {
        count[i] += count[i - 1];
    }
    for (long long i = 0; i < size; i++) {
        suffixArray[count[sourse[i]] - 1] = i;
        count[sourse[i]]--;
    }
    numberClasses = 0;
    classes[suffixArray[0]] = 0;
    for (long long i = 1; i < size; i++) {
        if (sourse[suffixArray[i]] != sourse[suffixArray[i - 1]]) {
            numberClasses++;
        }
        classes[suffixArray[i]] = numberClasses;
    }
    numberClasses++;

}

void SuffixArray::makeSuffixArray() {
    std::vector<long long> newSuffixArray(size, 0);
    for (long long length = 1; length < size;) {
        for (long long i = 0; i < size; i++) {
            newSuffixArray[i] = suffixArray[i] - length;
            if (newSuffixArray[i] < 0) {
                newSuffixArray[i] += size;
            }
        }
        std::vector<long long> count(numberClasses, 0);
        for (long long i = 0; i < size; i++) {
            count[classes[i]]++;
        }

        for (long long i = 1; i < numberClasses; i++) {
            count[i] += count[i - 1];
        }
        for (long long i = size - 1; i >= 0; i--)
            suffixArray[--count[classes[newSuffixArray[i]]]] = newSuffixArray[i];
        std::vector<long long> newClasses(size, 0);
        numberClasses = 0;
        newClasses[suffixArray[0]] = 0;
        for (long long i = 1; i < size; i++) {
            long long middle = (suffixArray[i] + length) % size;
            long long prevMiddle = (suffixArray[i - 1] + length) % size;
            if (classes[suffixArray[i]] != classes[suffixArray[i - 1]] ||
                classes[middle] != classes[prevMiddle]) {
                numberClasses++;
            }
            newClasses[suffixArray[i]] = numberClasses;
        }
        for (long long i = 0; i < size; i++) {
            classes[i] = newClasses[i];
        }
        numberClasses++;
        length = length + length;
    }
}


std::pair<int, int> kCommonPrefix(const std::vector<long long> &suffixArray, const std::string &str, int firstLen, long long position) {
    int size = str.size();
    std::vector<long long> suffixPosition(size, 0);
    for (int i = 0; i < size; i++) {
        suffixPosition[suffixArray[i]] = i;
    }
    std::vector<long long> lcp(size, 0);
    long long k = 0;
    long long a = 0;
    for (int i = 0; i < size; i++) {
        k = std::max(k - 1, a);
        if (suffixPosition[i] == size - 1) {
            lcp[size - 1] = 0;
            k = 0;
        } else {
            long long nextSuffix = suffixArray[suffixPosition[i] + 1];
            while ((i + k < size) && (nextSuffix + k < size) && (str[i + k] == str[nextSuffix + k])) {
                k++;
            }
            lcp[suffixPosition[i]] = k;
        }
    }
    std::vector<bool> category(suffixArray.size(), 0);
    for (int i = 0; i < suffixArray.size(); i++) {
        if (suffixArray[i] < firstLen) {
            category[i] = true;
        }
    }
    std::pair<long long, long long> ourStr;
    long long result = 0;
    long long minlcp = 0;
    for (int i = 2; i < size - 1; i++) {
        if ((category[i] || category[i + 1]) && !(category[i] && category[i + 1])) {
            if (lcp[i] > minlcp) {
                result += lcp[i] - minlcp;
                if (result >= position) {
                    ourStr.first = suffixArray[i];
                    ourStr.second = lcp[i] - result + position;
                    return ourStr;
                }
                minlcp = lcp[i];
            } else {
                if (lcp[i] < minlcp) {
                    minlcp = lcp[i];
                }
            }
        } else {
            if (lcp[i] < minlcp) {
                minlcp = lcp[i];
            }
        }
    }
    if (result < position) {
        ourStr.first = -1;
    }
    return ourStr;
}

int main() {
    std::string firstString;
    std::string secondString;
    std::cin >> firstString;
    std::cin >> secondString;
    long long k = 0;
    std::cin >> k;
    std::vector<long long> suf(firstString.size() + secondString.size() + 2, 0);
    suf = SuffixArray(firstString + '#' + secondString).giveSuffixArray();
    std::string thirdString;
    thirdString = firstString + '#' + secondString + '$';
    std::pair<long long, long long> result = kCommonPrefix(suf, thirdString, firstString.size(), k);
    if (result.first != -1) {
        std::string answer = "";
        for (long long i = result.first; i < result.first + result.second; i++) {
            answer += thirdString[i];
        }
        std::cout << answer;
    } else {
        std::cout << -1;
    }
    return 0;
}
