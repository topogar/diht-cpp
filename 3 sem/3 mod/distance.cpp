/* Даны два отрезка в пространстве (x1, y1, z1) - (x2, y2, z2) и (x3, y3, z3) - (x4, y4, z4). Найдите расстояние между отрезками. */

#include <iostream>
#include <math.h>
using namespace std;

const double EPS = 1e-06;

class Vector {
private:
    double x;
    double y;
    double z;
public:
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z) {} ;
    double retX() const {
        return x;
    }

    double retY() const {
        return y;
    }

    double retZ() const {
        return z;
    }

    Vector& operator=(const Vector& other);

    Vector operator*(double value);

    Vector operator+(Vector& _vector);

    Vector operator-(Vector& _vector);

    Vector& operator*=(double value);

    Vector& operator+=(const Vector& _vector);

    Vector& operator-=(const Vector& _vector);

    double dotProd(Vector& _vector);

    double Len();
};

Vector& Vector::operator=(const Vector &other) {
    if (this == &other) {
        return *this;
    } else {
        x = other.retX();
        y = other.retY();
        z = other.retZ();
        return *this;
    }
}

Vector Vector::operator*(double value) {
    Vector summ(this->x, this->y, this->z);
    summ.x *= value;
    summ.y *= value;
    summ.z *= value;
    return summ;
}

Vector Vector::operator+(Vector &_vector) {
    Vector summ(this->x, this->y, this->z);
    summ.x += _vector.retX();
    summ.y += _vector.retY();
    summ.z += _vector.retZ();
    return summ;
}

Vector Vector::operator-(Vector &_vector) {
    Vector summ(this->x, this->y, this->z);
    summ.x -= _vector.retX();
    summ.y -= _vector.retY();
    summ.z -= _vector.retZ();
    return summ;
}

Vector& Vector::operator*=(double value) {
    this->x *= value;
    this->y *= value;
    this->z *= value;
    return *this;
}

Vector& Vector::operator+=(const Vector &_vector) {
    this->x += _vector.retX();
    this->y += _vector.retY();
    this->z += _vector.retZ();
    return *this;
}

Vector& Vector::operator-=(const Vector &_vector) {
    this->x -= _vector.retX();
    this->y -= _vector.retY();
    this->z -= _vector.retZ();
    return *this;
}

double Vector::dotProd(Vector &_vector) {
    return (this->x * _vector.retX()) + (this->y * _vector.retY() + (this->z * _vector.retZ()));
}

double Vector::Len() {
    return sqrt(this->dotProd(*this));
}

class distanceBetweenSegments {
private:
    Vector firstVector;
    Vector secondVector;
    Vector basicVector;
    double firstParameter;
    double secondParameter;
    double distance;
    double lenSegment() {
        Vector result = basicVector;
        result += firstVector * firstParameter;
        result -= secondVector * secondParameter;
        return result.Len();
    }
    double segmentIntersectionDistance() {
        double scF = firstVector.dotProd(firstVector);
        double scS = secondVector.dotProd(secondVector);
        double scFS = firstVector.dotProd(secondVector);
        double scFB = firstVector.dotProd(basicVector);
        double scSB = secondVector.dotProd(basicVector);
        double denominator = scF * scS - scFS * scFS;
        double fNum;
        double sNum;
        double fDen;
        double sDen;
        if (denominator < EPS) { // То есть прямые параллельны
            // Могу взять за первый параметр любое удобное число, а второй - вывести через матан
            fNum = 0.0;
            fDen = 1.0;
            sNum = scSB;
            sDen = scS;
        } else {
            // Не параллельны, приходится пользоваться матаном
            fNum = scFS * scSB - scS * scFB;
            sNum = scF * scSB - scFS * scFB;
            fDen = denominator;
            sDen = denominator;
        }
        if (fNum <= 0.0) { // Значит, первый параметр < 0 => = 0
            fNum = 0.0;
            sNum = scSB;
            sDen = scS;
        } else if (fNum >= fDen){ // Значит, первый параметр > 1 => = 1
            fNum = fDen;
            sNum = scSB + scFS;
            sDen = scS;
        }
        if (sNum <= 0.0) { //Значит, второй параметр < 0 => = 0, пересчитываем первый
            sNum = 0.0;
            fDen = scF;
            fNum = max(0.0, min(-scFB, fDen));
        } else if (sNum >= sDen) { //Значит, второй параметр > 1 => = 1, пересчитываем первый
            sNum = sDen;
            fDen = scF;
            fNum = max(0.0, min(scFS - scFB, scF));
            }
        if (fabs(fNum) < EPS) {
            firstParameter = 0;
        } else {
            firstParameter = fNum / fDen;
        }
        if (fabs(sNum) < EPS) {
            secondParameter = 0;
        } else {
            secondParameter = sNum / sDen;
        }
        return lenSegment();
    }
public:
    distanceBetweenSegments(Vector& firstBegin, Vector& firstEnd, Vector& secondBegin, Vector& secondEnd) :
            firstVector(firstEnd - firstBegin), secondVector(secondEnd - secondBegin), basicVector(firstBegin - secondBegin) {
        distance = segmentIntersectionDistance();
    }
    double retDistance() {
        return distance;
    }
};

int main() {
    double x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4;
    cin >> x1 >> y1 >> z1 >> x2 >> y2 >> z2 >> x3 >> y3 >> z3 >> x4 >> y4 >> z4;
    Vector firstBeg(x1, y1, z1);
    Vector firstEnd(x2, y2, z2);
    Vector secondBeg(x3, y3, z3);
    Vector secondEnd(x4, y4, z4);
    distanceBetweenSegments distance(firstBeg, firstEnd, secondBeg, secondEnd);
    printf("%.8f", distance.retDistance());
    return 0;
}
