#include <vector>
#include <iostream>
#include <cmath>
#include <queue>
#include <algorithm>
using namespace std;

const double EPS = 1e-06;

class Vector {
private:
    double x;
    double y;
    double z;
public:
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z) {} ;
    double retX() const {
        return x;
    }

    double retY() const {
        return y;
    }

    double retZ() const {
        return z;
    }

    Vector& operator=(const Vector& other);

    Vector operator*(double value);

    Vector operator+(Vector& _vector);

    Vector operator-(const Vector& _vector);

    Vector& operator*=(double value);

    Vector& operator+=(const Vector& _vector);

    Vector& operator-=(const Vector& _vector);

    double dotProd(Vector& _vector);

    double Len();
};

Vector& Vector::operator=(const Vector &other) {
    if (this == &other) {
        return *this;
    } else {
        x = other.retX();
        y = other.retY();
        z = other.retZ();
        return *this;
    }
}

Vector Vector::operator*(double value) {
    Vector summ(this->x, this->y, this->z);
    summ.x *= value;
    summ.y *= value;
    summ.z *= value;
    return summ;
}

Vector Vector::operator+(Vector &_vector) {
    Vector summ(this->x, this->y, this->z);
    summ.x += _vector.retX();
    summ.y += _vector.retY();
    summ.z += _vector.retZ();
    return summ;
}

Vector Vector::operator-(const Vector &_vector) {
    Vector summ(this->x, this->y, this->z);
    summ.x -= _vector.retX();
    summ.y -= _vector.retY();
    summ.z -= _vector.retZ();
    return summ;
}

Vector& Vector::operator*=(double value) {
    this->x *= value;
    this->y *= value;
    this->z *= value;
    return *this;
}

Vector& Vector::operator+=(const Vector &_vector) {
    this->x += _vector.retX();
    this->y += _vector.retY();
    this->z += _vector.retZ();
    return *this;
}

Vector& Vector::operator-=(const Vector &_vector) {
    this->x -= _vector.retX();
    this->y -= _vector.retY();
    this->z -= _vector.retZ();
    return *this;
}

double Vector::dotProd(Vector &_vector) {
    return (this->x * _vector.retX()) + (this->y * _vector.retY() + (this->z * _vector.retZ()));
}

double Vector::Len() {
    return sqrt(this->dotProd(*this));
}

struct Face {
    int fPoint;
    int sPoint;
    int tPoint;
    Face(int _fPoint, int _sPoint, int _tPoint) : fPoint(_fPoint), sPoint(_sPoint), tPoint(_tPoint) {};
};

bool operator<(const Face a,const Face b) {
    if (a.fPoint != b.fPoint)
        return a.fPoint < b.fPoint ;
    if (a.sPoint != b.sPoint)
        return a.sPoint < b.sPoint ;
    return a.tPoint < a.tPoint ;
}

class convex3D {
private:
    vector<Vector> buf;

    vector<Face> faces;

    double calcAngle(int first, int second);

    double signedVolume(int first, int second, int third, int another);

    int findThirdPoint(int first, int second);

    Face firstFace();

    bool isBeen(int first, int second, int third);

    void orderFace(int i, const Vector& point);

public:
    explicit convex3D(const vector<Vector>& _buf) : buf(_buf) {};

    void buildConvex();

    void getAnswer();
};

double convex3D::calcAngle(int first, int second) {
    Vector edge = buf[second] - buf[first];
    return acos(edge.retX()/edge.Len());
    // Это угол между прилежащим и гипотенузой => искомый (как н/л углы)
}

double convex3D::signedVolume(int first, int second, int third, int another) {
    Vector SF = buf[second] - buf[first];
    Vector TF = buf[third] - buf[first];
    double xCoordinate = SF.retY() * TF.retZ() - SF.retZ() * TF.retY();
    double yCoordinate = SF.retZ() * TF.retX() - SF.retX() * TF.retZ();
    double zCoordinate = SF.retX() * TF.retY() - SF.retY() * TF.retX();
    Vector vecProd(xCoordinate, yCoordinate, zCoordinate);
    Vector AF = buf[another] - buf[first];
    return AF.dotProd(vecProd);
}

int convex3D::findThirdPoint(int first, int second) { //Выделил отдельно, так как придется не раз использовать
    int third;
    if (first != 0 && second != 0) {
        third = 0;
    } else if (first != 1 && second != 1) {
        third = 1;
    } else {
        third = 2;
    }
    for (int i = third; i < buf.size(); i++) {
        if (i != first && i != second) {
            double volume = signedVolume(first, second, third, i);
            if (volume < 0) {
                third = i;
            }
        }
    }
    return third;
}

Face convex3D::firstFace() {
    int firstVert = 0;
    for (int i = 1; i < buf.size(); i++) {
        if (buf[i].retX() < buf[firstVert].retX()) {
            firstVert = i;
        }
    }
    int secondVert = 0;
    if (secondVert == firstVert) {
        secondVert = 1;
    }
    for (int i = 1; i < buf.size(); i++) {
        if (secondVert != firstVert && calcAngle(firstVert, i) > calcAngle(firstVert, secondVert)) {
            secondVert = i;
        }
    }
    int thirdVert = findThirdPoint(firstVert, secondVert);
    faces.emplace_back(Face(firstVert, secondVert, thirdVert));
}

bool convex3D::isBeen(int first, int second, int third) {
    for (int i = 0; i < faces.size(); i++) {
        if ((faces[i].fPoint == first || faces[i].sPoint == first || faces[i].tPoint == first)
            && (faces[i].fPoint == second || faces[i].sPoint == second || faces[i].tPoint == second)
            && (faces[i].fPoint == third || faces[i].sPoint == third || faces[i].tPoint == third)) {
            return true;
        }
    }
    return false;
}

void convex3D::orderFace(int i, const Vector &point) {
    int minIndex = min(faces[i].fPoint, min(faces[i].sPoint, faces[i].tPoint));
    int second = 0;
    int third = 0;
    if (minIndex == faces[i].fPoint) {
        second = faces[i].sPoint;
        third = faces[i].tPoint;
    } else if (minIndex == faces[i].sPoint) {
        second = faces[i].fPoint;
        third = faces[i].tPoint;
    } else {
        second = faces[i].fPoint;
        third = faces[i].sPoint;
    }
    Vector SF = buf[second] - buf[minIndex];
    Vector FT = buf[third] - buf[minIndex];
    double xCoordinate = SF.retY() * FT.retZ() - SF.retZ() * FT.retY();
    double yCoordinate = SF.retZ() * FT.retX() - SF.retX() * FT.retZ();
    double zCoordinate = SF.retX() * FT.retY() - SF.retY() * FT.retX();
    Vector vecProduct(xCoordinate, yCoordinate, zCoordinate);
    Vector FP = buf[minIndex] - point;
    if (vecProduct.dotProd(FP) > 0) {
        faces[i].fPoint = minIndex;
        faces[i].sPoint = second;
        faces[i].tPoint = third;
    } else {
        faces[i].fPoint = minIndex;
        faces[i].sPoint = third;
        faces[i].tPoint = second;
    }
}

void convex3D::buildConvex() {
    firstFace();
    queue <Face> q;
    q.push(faces[0]);
    while (!q.empty()) {
        Face tmp = q.front();
        q.pop();
        vector<pair<int, int>> tmpEdges;
        tmpEdges.emplace_back(make_pair(tmp.sPoint, tmp.fPoint));
        tmpEdges.emplace_back(make_pair(tmp.fPoint, tmp.tPoint));
        tmpEdges.emplace_back(make_pair(tmp.tPoint, tmp.sPoint));
        for (int i = 0; i < 3; i++) {
            int first = tmpEdges[i].first;
            int second = tmpEdges[i].second;
            int third = findThirdPoint(first, second);
            if (!isBeen(first, second, third)) {
                faces.emplace_back(Face(first, second, third));
                q.push(Face(first, second, third));
            }
        }
    }
}

void convex3D::getAnswer() {
    Vector point(0, 0, 0);
    for (int i = 0; i < faces.size(); i++) {
        point += buf[i];
    }
    point *= 1.0/faces.size();
    for (int i = 0; i < faces.size(); i++) {
        orderFace(i, point);
    }
    sort(faces.begin(), faces.end());
    cout << faces.size() << '\n';
    for (int i = 0; i < faces.size(); i++) {
        cout << 3 << " " << faces[i].fPoint << " " << faces[i].sPoint << " " << faces[i].tPoint << '\n';
    }
}

int main() {
    int n;
    cin >> n;
    for (int i = 0; i < n; i++) {
        vector<Vector> figure;
        int m;
        cin >> m;
        vector<Vector> buf;
        for (int i = 0; i < m; i++) {
            double x = 0;
            double y = 0;
            double z = 0;
            cin >> x >> y >> z;
            buf.emplace_back(Vector(x, y, z));
        }
        convex3D conv(buf);
        conv.buildConvex();
        conv.getAnswer();
    }
    return 0;
}
