#include <iostream>
#include <vector>
#include <string>
using namespace std;

const int NUMBER_RUSSIAN_LETTERS = 26;

vector<vector<int>> helpZ(vector<int>& zFunc) {
    vector<vector<int>> newZ(zFunc.size());
    for (int i = 1; i < zFunc.size(); i++) {
        if (zFunc[i] != 0) {
            newZ[zFunc[i] + i - 1].push_back(zFunc[i]);
        }
    }
    return newZ;
}

vector<int> prFunc(string& str, string& tmp) {
    str = tmp + '@' + str;
    vector<int> prFunc(str.length(), 0);
    for (int i = 1; i < str.length(); i++) {
        int len = prFunc[i - 1];
        for (len; len > 0; len = prFunc[len - 1]) {
            if (str[len] == str[i]) {
                break;
            }
        }
        if (str[len] == str[i]) {
            len++;
        }
        prFunc[i] = len;
    }
    return prFunc;
}

vector <int> zToPr(const vector<int>& zFunc) {
    vector<int> prFunc(zFunc.size(), 0);
    for (int i = 1; i < zFunc.size(); i++) {
        for (int j = zFunc[i] - 1; j >= 0; j--) {
            if (prFunc[i + j] == 0) {
                prFunc[i + j] = j + 1;
            } else {
                break;
            }
        }
    }
    return prFunc;
}

vector <int> PrToZ(const vector<int>& prFunc) {
    vector<int> zFunc(prFunc.size());
    for (int i = 1; i < prFunc.size(); i++) {
        if (prFunc[i] > 0) {
            zFunc[i - prFunc[i] + 1] = prFunc[i];
        }
    }
    zFunc[0] = prFunc.size();
    int i = 1;
    while (i < prFunc.size()) {
        int k = i;
        if (zFunc[i] > 0) {
            for (int j = 1; j < zFunc[i]; j++) {
                if (zFunc[i + j] > zFunc[j]) {
                    break;
                }
                zFunc[i + j] = min(zFunc[i] - j, zFunc[j]);
                k = i + j;
            }
        }
        i = k + 1;
    }
    zFunc[0] = 0;
    return zFunc;
}

string restoreStrZ(vector<int> zFunc) {
    if (zFunc.size() == 0) {
        return "";
    }
    zFunc[0] = 0;
    vector<vector<int>> newZ = helpZ(zFunc);
    int prefixLength = 0;
    int j = 0;
    string res = "a";
    for (int i = 1; i < zFunc.size(); i++) {
        int k = 0;
        if (zFunc[i] == 0 && prefixLength == 0) {
            vector<bool> used(NUMBER_RUSSIAN_LETTERS, false);
            used[int(res[k]) - int('a')] = true;
            while (k < newZ[i - 1].size()) {
                used[int(res[newZ[i - 1][k]]) - int('a')] = true;
                k++;
            }
            for (int j = 0; j < NUMBER_RUSSIAN_LETTERS; j++) {
                if (!used[j]) {
                    res += char(int('a') + j);
                    break;
                }
            }
        }
        if (zFunc[i] > prefixLength) {
            prefixLength = zFunc[i];
            j = 0;
        };
        if (prefixLength > 0) {
            res += res[j];
            j++;
            prefixLength--;
        }
    }
    return res;
}


int main() {
    vector<int> zFunc;
    int s;
    while (cin >> s) {
        zFunc.push_back(s);
    }
    cout << restoreStrZ(zFunc);
    return 0;
}
