#include <iostream>
#include <vector>
#include <string>
using namespace std;

const int NUMBER_RUSSIAN_LETTERS = 26;

string restoreStr(vector<int>& prFunc) {
    string result = "a";
    // Так как точно начинается с буквы а
    for (int i = 1; i < prFunc.size(); i++) {
        if (prFunc[i] != 0) {
            result += result[prFunc[i] - 1];
            // Если не равна нулю, это единственное, что мы можем сделать
        } else {
            vector<bool> used(NUMBER_RUSSIAN_LETTERS, false);
            // Отмечаю буквы, которые точно не подойдут
            int k = prFunc[i - 1];
            used[int(result[k]) - int('a')] = true;
            while (k != 0) {
                used[int(result[k]) - int('a')] = true;
                k = prFunc[k - 1];
            }
            for (int j = 0; j < 26; j++) {
                if (!used[j]) {
                    result += char(int('a') + j);
                    break;
                }
            }
        }
    }
    return result;
}
int main() {
    vector<int> prFunc;
    int s;
    while (cin >> s) {
        prFunc.push_back(s);
    }
    cout << restoreStr(prFunc);
    return 0;
}
