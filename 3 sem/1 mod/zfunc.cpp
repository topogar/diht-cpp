#include <iostream>
#include <vector>
#include <string>
using namespace std;

class ZFunc {
private:
    string tmp;
    string text;
    vector <int> values;
    int size;
    int pos;
    int leftPos;
    int rightPos;
    void init();
    bool readStr();
    void zFunc();
public:
    ZFunc(string str) : tmp(str + "#"), size(str.length()), leftPos(0), rightPos(0),
                        pos(str.length()), values(str.length() + 1, 0), text("") {
        init();
    }
    void search();
};

void ZFunc::init() {
    for (int i = 1; i < tmp.length(); i++) {
        if (i <= rightPos) {
            values[i] = min(rightPos - i + 1, values[i - leftPos]);
        }

        while (i + values[i] < tmp.length() && tmp[values[i]] == tmp[values[i] + i]) {
            values[i]++;
        };
        if (i + values[i] - 1 > rightPos) {
            leftPos = i, rightPos = values[i] + i - 1;
        }
    }
}

void ZFunc::search() {
    bool cont = true;
    char ch = cin.get();
    while(cont) {
        cont = readStr();
        zFunc();
    }
}

void ZFunc::zFunc() {
    int begin = size + 1;
    int end = text.length() - size + 1;
    int k = 0;
    for (int i = begin; i < end; i++) {
        pos++;
        if (pos <= rightPos) {
            k = min(rightPos - pos + 1, values[pos - leftPos]);
        }
        while (i + k < text.length() && text[k] == text[k + i]) {
            k++;
        };
        if (pos + k - 1 > rightPos) {
            leftPos = pos, rightPos = k + pos - 1;
        }
        if (k == size) {
            cout << pos - size - 1 << " ";
        }
    }
    string newText = "";
    for (int i = 0; i < size - 1; i++) {
        newText += text[end + i];
    }
    text = newText;
}

bool ZFunc::readStr() {
    text = tmp + text;
    char symb;
    for (int i = 0; i < 2 * size; i++) {
        if (cin.get(symb) && symb != '\n') {
            text += symb;
        } else {
            return false;
        }
    }
    return true;
}

int main() {
    string T;
    cin >> T;
    ZFunc str = ZFunc(T);
    str.search();
    return 0;
}
