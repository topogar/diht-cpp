/* Даны неотрицательные целые числа n, k и массив целых чисел из диапазона [0..109] размера n.
Требуется найти k-ю порядковую статистику. т.е. напечатать число, которое бы стояло на позиции с индексом k ∈[0..n-1] в отсортированном массиве.
Напишите нерекурсивный алгоритм.
Требования к дополнительной памяти: O(n).
Требуемое среднее время работы: O(n).
Функцию Partition следует реализовывать методом прохода двумя итераторами в одном направлении.
Описание для случая прохода от начала массива к концу:
Выбирается опорный элемент.
Опорный элемент меняется с последним элементом массива.
Во время работы Partition в начале массива содержатся элементы, не бОльшие опорного. Затем располагаются элементы, строго бОльшие опорного. В конце массива лежат нерассмотренные элементы. Последним элементом лежит опорный.
Итератор (индекс) i указывает на начало группы элементов, строго бОльших опорного.
Итератор j больше i, итератор j указывает на первый нерассмотренный элемент.
Шаг алгоритма. Рассматривается элемент, на который указывает j. Если он больше опорного, то сдвигаем j. Если он не больше опорного, то меняем a[i] и a[j] местами, сдвигаем i и сдвигаем j.
В конце работы алгоритма меняем опорный и элемент, на который указывает итератор i. */

#include <vector>
#include <iostream>
using namespace std;

bool Comparator(int a, int b) {
	if (a >= b) return true;
	return false;
}

template <typename Iterator, typename cmp>
Iterator Median(Iterator first, Iterator second, Iterator third, cmp comparator) {
	if (comparator(*first, *second)) {
		Iterator c = first;
		first = second;
		second = c;
	};
	if (comparator(*third, *first)) {
		if (!comparator(*third, *second)) {
			Iterator c = second;
			second = third;
			third = c;
		};
	}
	else {
		Iterator c = first;
		first = third;
		third = c;
		c = second;
		second = third;
		third = c;
	};
	return second;
};

template <typename Iterator, typename cmp>
Iterator Partition(Iterator begin, Iterator end, cmp comparator) {
	Iterator median = begin + distance(begin, end - 1) / 2;
	Iterator pivotPos = Median(begin, median, end - 1, comparator);
	swap(*begin, *pivotPos);
	Iterator i = end - 1;
	Iterator j = end - 1;
	while (j > begin) {
		while (!comparator(*j, *begin) && j > begin) {
			j--;
		};
		while (comparator(*j, *begin) && j > begin) {
			swap(*(j--), *(i--));
		};
	};
	swap(*i, *begin);
	return i;
};

template <typename Iterator, typename cmp>
int FindK(Iterator begin, Iterator end, int k, cmp comparator) {
	auto pivotPos = begin;
	auto left = begin;
	auto right = end;
	while (left < right) {
		Iterator pivotPos = Partition(left, right, comparator);
		if (pivotPos == begin + k) {
			return *pivotPos;
		};
		if (pivotPos > begin + k) {
			right = pivotPos;
		}
		else {
			left = pivotPos + 1;
		};
	};
	return *pivotPos;
};

int main() {
	int n;
	int k;
	cin >> n >> k;
	vector <int> numbers(n);
	for (int i = 0; i < n; i++) {
		cin >> numbers[i];
	};
	cout << FindK(numbers.begin(), numbers.end(), k, Comparator);
	return 0;
}
