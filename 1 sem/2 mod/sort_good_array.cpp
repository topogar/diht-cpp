/*Дана последовательность целых чисел a1...an и натуральное число k, такое что для любых i, j: если j >= i + k, то a[i] <= a[j]. Требуется отсортировать последовательность. Последовательность может быть очень длинной. Время работы O(n * log(k)). Доп. память O(k). Использовать слияние. */

#include <iostream>
#include <vector>
using namespace std;

bool Less(int a, int b) {
	if (a < b) {
		return true;
	}
	else {
		return false;
	}
}

template <typename Iterator, typename Cmp>
void Merge(Iterator left, Iterator mid, Iterator right, Iterator beginhelp, Cmp comparator) {
	int it1 = 0;
	int it2 = 0;
	while ((left + it1 < mid) && (mid + it2 < right)) {
		if (comparator(*(left + it1), *(mid + it2))) {
			*(beginhelp + it1 + it2) = *(left + it1);
			it1++;
		}
		else {
			*(beginhelp + it1 + it2) = *(mid + it2);
			it2++;
		};
	};
	while (left + it1 < mid) {
		*(beginhelp + it1 + it2) = *(left + it1);
		it1++;
	};
	while (mid + it2 < right) {
		*(beginhelp + it1 + it2) = *(mid + it2);
		it2++;
	};
	for (int i = 0; i < it1 + it2; i++) {
		*(left + i) = *(beginhelp + i);
	};
};

template <typename Iterator>
void MergeSort(Iterator left, Iterator right, Iterator beginhelp) {
	if (left + 1 >= right) {
		return;
	}
	Iterator mid = left + distance(left, right) / 2;
	MergeSort(left, mid, beginhelp);
	MergeSort(mid, right, beginhelp);
	Merge(left, mid, right, beginhelp, Less);
}

int main() {
	int n = 0;
	int k = 0;
	cin >> n;
	cin >> k;
	vector <int> res(2 * k);
	vector <int> First(2 * k);
	auto beginRes = res.begin();
	for (int i = k; i < 2 * k; i++) {
		int a = 0;
		cin >> a;
		First[i] = a;
	}
	for (int i = 0; i < (n - k) / k; i++) {
		for (int j = 0; j < k; j++) {
			cin >> First[j];
		}
		MergeSort(First.begin(), First.begin() + 2 * k, beginRes);
		for (int j = 0; j < k; j++) {
			cout << First[j] << " ";
			First[j] = -1;
		};
	};
	for (int i = 0; i < n % k; i++) {
		cin >> First[k - 1 - i];
	};
	MergeSort(First.begin() + k - 1 - (n % k), First.begin() + 2 * k, beginRes);
	for (int i = k - (n % k); i < 2 * k; i++) {
		cout << First[i] << " ";
	};
	return 0;
}

