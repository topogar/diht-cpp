/* Дан массив неотрицательных целых 64-разрядных чисел. Количество чисел не больше 1000000. Отсортировать массив методом MSD по битам (бинарный QuickSort). */

#include <iostream>
#include <vector>
using namespace std;

int bit(long long a, int k) {
	return ((a >> k) & 1);
}

template<typename Iterator>
void BinaryQuickSort(Iterator begin, Iterator end, int k) {
	if (end <= begin || k < 0) {
		return;
	};
	auto i = begin;
	auto j = end;
	while (i != j) {
		while (i < j && bit(*i, k) == 0) {
			i++;
		};
		while (j > i && bit(*j, k) == 1) {
			j--;
		};
		swap(*i, *j);
	};
	if (bit(*end, k) == 0) {
		j++;
	}
	BinaryQuickSort(begin, j - 1, k - 1);
	BinaryQuickSort(j, end, k - 1);
}


int main() {
	int n = 0;
	cin >> n;
	vector <long long> a(n);
	for (int i = 0; i < n; i++) {
		cin >> a[i];
	};
	BinaryQuickSort(a.begin(), a.end() - 1, 63);
	for (int i = 0; i < n; i++) {
		cout << a[i] << " ";
	};
	return 0;
}
