/* Дан массив целых чисел в диапазоне [0..109]. Размер массива кратен 10 и ограничен сверху значением 2.5 * 107 элементов. Все значения массива являются элементами псевдо-рандомной последовательности. Необходимо отсортировать элементы массива за минимально время и вывести каждый десятый элемент отсортированной последовательности.
Реализуйте сортировку, основанную на QuickSort.
Минимальный набор оптимизаций, который необходимо реализовать:
1. Оптимизация ввода/вывода
2. Оптимизация выбора опорного элемента
3. Оптимизация Partition
4. Оптимизация рекурсии
5. Оптимизация концевой рекурсии */

#include <iostream>
#include <vector>
#include <stack>
using namespace std;

bool pomparator(int a, int b) {
	if (a < b) return true;
	return false;
}
template <typename Iterator, typename cmp>
Iterator Median(Iterator begin, Iterator end, cmp comparator) {
	Iterator half = begin + distance(begin, end) / 2;
	if (comparator(*begin, *half)) {
		if (*end < *begin) {
			return begin;
		}
		else if (comparator(*end, *half)) {
			return end;

		}
		return half;
	}
	else {
		if (comparator(*end, *half)) {
			return half;
		}
		else
			if (comparator(*end, *begin)) {
				return end;
			}
		return begin;
	}
};

template <typename Iterator, typename cmp>
Iterator Partition(Iterator begin, Iterator end, cmp comparator) {
	Iterator pivotPos = Median(begin, end, comparator);
	swap(*end, *pivotPos);
	auto i = begin;
	auto j = begin;
	while (j < end) {
		if (*j <= *end) {
			swap(*(i++), *j);
		}
		j++;
	}
	swap(*i, *end);
	return i;
};

template<typename Iterator>
void PushTwoElement(stack <Iterator> &s, Iterator begin, Iterator end) {
	s.push(end);
	s.push(begin);
}

template <typename Iterator, typename cmp>
void quickSort(Iterator begin, Iterator end, cmp comparator) {
	stack< Iterator > s;
	PushTwoElement(s, begin, end);
	while (!s.empty()) {
		begin = s.top();
		s.pop();
		end = s.top();
		s.pop();
		if (end <= begin) {
			continue;
		}
		Iterator i = Partition(begin, end, comparator);
			PushTwoElement(s, begin, i - 1);
			PushTwoElement(s, i + 1, end);
	}
}

int main() {
	ios_base::sync_with_stdio(false);
	cin.tie(false);
	vector <int> c;
	int n = 0;
	int a = 0;
	while (cin >> a) {
		c.push_back(a);
		n++;
	};
	quickSort(c.begin(), c.end() - 1, pomparator);
	for (int i = 9; i < n; i += 10) {
		cout << c[i] << " ";
	};
	return 0;
}

