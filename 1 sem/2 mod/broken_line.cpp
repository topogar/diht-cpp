/* Задано N точек на плоскости. Указать (N−1)-звенную несамопересекающуюся незамкнутую ломаную, проходящую через все эти точки.
Указание: стройте ломаную в порядке возрастания x-координаты. Если имеются две точки с одинаковой x-координатой, то расположите раньше ту точку, у которой y-координата меньше. */

#include <iostream>
#include <vector>
#include <stack>
using namespace std;

template <typename Iterator>
Iterator Median(Iterator begin, Iterator end) {
	Iterator half = begin + distance(begin, end) / 2;
	if (*begin < *half) {
		if (*end < *begin) {
			return begin;
		}
		else if (*end < *half) {
			return end;

		}
		return half;
	}
	else {
		if (*end < *half) {
			return half;
		}
		else
			if (*end < *begin) {
				return end;
			}
		return begin;
	}
};

template <typename Iterator>
Iterator Partition(Iterator begin, Iterator end) {
	Iterator pivotPos = Median(begin, end);
	swap(*end, *pivotPos);
	auto i = begin;
	auto j = begin;
	while (j < end) {
		if (*j <= *end) {
			swap(*(i++), *j);
		}
		j++;
	}
	swap(*i, *end);
	return i;
};

template<typename Iterator>
void PushTwoElement(stack <Iterator> &s, Iterator begin, Iterator end) {
	s.push(end);
	s.push(begin);
}

template <typename Iterator>
void quickSort(Iterator begin, Iterator end) {
	stack< Iterator > s;
	PushTwoElement(s, begin, end);
	while (!s.empty()) {
		begin = s.top();
		s.pop();
		end = s.top();
		s.pop();
		if (end <= begin) {
			continue;
		}
		Iterator i = Partition(begin, end);
		//if (i - 1 > end - i) {
			PushTwoElement(s, begin, i - 1);
			PushTwoElement(s, i + 1, end);
		/*}
		else {
			PushTwoElement(s, i + 1, end);
			PushTwoElement(s, begin, i - 1);
		}*/
	}
}

int main() {
	ios_base::sync_with_stdio(false);
	cin.tie(false);
	vector <int> c;
	int n = 0;
	int a = 0;
	while (cin >> a) {
		c.push_back(a);
		n++;
	};
	quickSort(c.begin(), c.end() - 1);
	for (int i = 9; i < n; i += 10) {
		cout << c[i] << " ";
	};
	return 0;
}
