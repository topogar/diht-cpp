/* В круг выстроено N человек, пронумерованных числами от 1 до N. Будем исключать каждого k-ого до тех пор, пока не уцелеет только один человек.

Например, если N=10, k=3, то сначала умрет 3-й, потом 6-й, затем 9-й, затем 2-й, затем 7-й, потом 1-й, потом 8-й, за ним - 5-й, и потом 10-й. Таким образом, уцелеет 4-й.

Необходимо определить номер уцелевшего.

N, k ≤ 10000. */

#include <iostream> 
using namespace std; 

int DEAD (int n, int k) {
    if (n == 1) {
        return 1;
    }
    else {
        int i = 2;
        int c = 1;
	    while (i < n + 1) {
	    	c = (c + k - 1) % i  + 1;
	    	i = i + 1;
        }
		return c;
    }
}

int main() 
{
    int n = 0;
    int k = 0;
    int life = 1;
    cin >> n;
    cin >> k;
    life = DEAD (n, k);
    cout << life;
	return 0; 
}
