/* Реализовать очередь с динамическим зацикленным буфером.

Обрабатывать команды push back и pop front.

В первой строке количество команд n. n ≤ 1000000.

Каждая команда задаётся как 2 целых числа: a b.

a = 2 - pop front
a = 3 - push back

Если дана команда pop front, то число b - ожидаемое значение. Если команда pop front вызвана для пустой структуры данных, то ожидается “-1”. */

#include <iostream>
#include <algorithm>
using namespace std;

class CQueue {
public:
	int* buffer;
	int bufferSize;
	int head;
	int tail;
	CQueue(int size);
	~CQueue() {
		delete[] buffer;
	}
	void PushBack(int a);
	int PopFront();
	int FirstElement();
};

CQueue::CQueue(int size) :
	bufferSize(size),
	head(0),
	tail(0)
{
	buffer = new int[bufferSize];
}

void CQueue::PushBack(int a)
{
	buffer[tail % bufferSize] = a;
	if ((tail + 1) % bufferSize == head) {
		int newBufferSize = bufferSize * 2;
		int* newBuffer = new int[newBufferSize];
		int j = 0;
		for (int i = head; i < bufferSize; ++i) {
			newBuffer[j] = buffer[i];
			++j;
		}
		for (int i = 0; i < head; ++i) {
			newBuffer[j] = buffer[i];
			++j;
		}
		delete[] buffer;
		buffer = newBuffer;
		bufferSize = newBufferSize;
		tail = j - 1;
		head = 0;
	}
	tail = (tail + 1) % bufferSize;
}

int CQueue::PopFront()
{
	if (head == tail) {
		return -1;
	};
	int result = buffer[head];
	head = (head + 1) % bufferSize;
	return result;
}

int CQueue::FirstElement()
{
	if (head == tail) {
		return -1;
	};
	int result = buffer[head];
	return result;
}

int main()
{
	int n = 0;
	int truth = 1;
	cin >> n;
	CQueue spisok(6);
	for (int i = 1; i < n + 1; ++i) {
		int a = 0;
		int b = 0;
		cin >> a >> b;
		if (a == 2) {
			if (b != spisok.FirstElement()) {
			truth = 0;
			}
			spisok.PopFront();
		}
		if (a == 3) {
			spisok.PushBack(b);
		}
	}
	if (truth == 0) {
		cout << "NO";
	}
	else cout << "YES";
	return 0;
}
