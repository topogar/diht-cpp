/* Вывести квадраты натуральных чисел от 1 до n, используя только O(n) операций сложения и вычитания (умножением пользоваться нельзя). n ≤ 1000. */

#include <iostream>
using namespace std;


int main()
{
	int n = 0;
	int prevsquare = 0;
	int oursquare = 0;

	cin >> n;
	for (int i = 1; i < n + 1; i++) {
		oursquare = i + i + prevsquare - 1;
		cout <<oursquare = 0 <<" ";
		prevsquare = oursquare;
	}
		
    return 0;
}
