/* Дан отсортированный массив различных целых чисел A[0..n-1] и массив целых чисел B[0..m-1]. Для каждого элемента массива B[i] найдите минимальный индекс элемента массива A[k], ближайшего по значению к B[i]. n ≤ 110000, m ≤ 1000. Время работы поиска для каждого элемента B[i]: O(log(k)). */

#include <iostream>
using namespace std;

int estimateLowerBound(const int *sorted, int searchedElement, int n, int estimate) {
	while (estimate < n && searchedElement > sorted[estimate]) {
		estimate = estimate * 2;
	}
	return estimate;
}
int adjustToNearestNeighbor(const int* sorted, int sortedSize, int lowerBound, int searchKey) {
	if ((lowerBound > 0 && (searchKey - sorted[lowerBound - 1] <= sorted[lowerBound] - searchKey)) || lowerBound == sortedSize) {
		return lowerBound - 1;
	}
	return lowerBound;
}

int lowerBound(const int *sortedArray, int arrayLen, int searchedElement, int estimate) {
	int leftIndex = estimate/2;
	if (estimate > arrayLen) { estimate = arrayLen; }
	int rightIndex = estimate;
	while (leftIndex + 1 < rightIndex) {
		const int middle = (leftIndex + rightIndex) / 2;
		if (sortedArray[middle] < searchedElement) {
			leftIndex = middle;
		}
		else {
			rightIndex = middle;
		}
	}
	return rightIndex;

}

int main()
{
	int n = 0;
	int m = 0;
	cin >> n;
	int *sorted = new int[n];
	for (int i = 0; i < n; ++i) {
		cin >> sorted[i];
	}
	cin >> m;
	int *unsorted = new int[m];
	for (int i = 0; i < m; ++i) {
		cin >> unsorted[i];
	}
	for (int i = 0; i < m; ++i) {
		int estimate = estimateLowerBound(sorted, unsorted[i], n, 1);
		int requiredElement = lowerBound(sorted, n, unsorted[i], estimate);
		requiredElement = adjustToNearestNeighbor(sorted, n, requiredElement, unsorted[i]);
		estimate = 1;
		cout << requiredElement << ' ';
	}
	return 0;
}
