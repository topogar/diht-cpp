/* На вокзале есть некоторое количество тупиков, куда прибывают электрички. Этот вокзал является их конечной станцией. Дано расписание движения электричек, в котором для каждой электрички указано время ее прибытия, а также время отправления в следующий рейс. Электрички в расписании упорядочены по времени прибытия. Когда электричка прибывает, ее ставят в свободный тупик с минимальным номером. При этом если электричка из какого-то тупика отправилась в момент времени X, то электричку, которая прибывает в момент времени X, в этот тупик ставить нельзя, а электричку, прибывающую в момент X+1 — можно. В данный момент на вокзале достаточное количество тупиков для работы по расписанию. Напишите программу, которая по данному расписанию определяет, какое минимальное количество тупиков требуется для работы вокзала.

Вначале вводится n - количество электричек в расписании. Затем вводится n строк для каждой электрички, в строке - время прибытия и время отправления. Время - натуральное число от 0 до 1 000 000 000. Строки в расписании упорядочены по времени прибытия. */

#include <iostream>
using namespace std;

struct train {
	int arrivalTime = 0;
	int departureTime = 0;
};

class heap {
public:
	train* trains;
	int bufferSize = 0;
	int size = 0;
	int k = 0;

	heap(int buffersize) : bufferSize(buffersize)
	{
		trains = new train[bufferSize];
	}

	~heap() {
		delete[] trains;
	}

	train getMinimum() {
		return trains[0];
	}

	void add(train element);
	bool isEmpty();
	void swap(train& first, train& second);
	void siftDown(int i);
	void siftUp(int i);
	void extractMin();
	void doubleBuffer();

};

void heap::doubleBuffer() {
	int newBufferSize = bufferSize * 2;
	train* newTrains = new train[newBufferSize];
	for (int i = 0; i < bufferSize; i++) {
		newTrains[i] = trains[i];
	}
	delete[] trains;
	trains = newTrains;
	bufferSize = newBufferSize;
}

void heap::add(train element) {
	if (size + 1 >= bufferSize) {
		doubleBuffer();
	}
	trains[size] = element;
	siftUp(size);
	size = size + 1;
	if (size > k) {
		k = k + 1;
	}
}

bool heap::isEmpty() {
	return (size == 0);
}

void heap::swap(train& first, train& second) {
	train coc = first;
	first = second;
	second = coc;
}

void heap::siftUp(int index) {
	while (index > 0) {
		int parent = (index - 1) / 2;
		if (trains[index].departureTime > trains[parent].departureTime) {
			return;
		}
		swap(trains[index], trains[parent]);
		index = parent;
	}
}

void heap::siftDown(int i) {
	int left = 2 * i + 1;
	int right = 2 * i + 2;
	int min = i;
	if (left < size + 1 && trains[left].departureTime < trains[i].departureTime) {
		min = left;
	}
	if (right < size + 1 && trains[right].departureTime < trains[min].departureTime) {
		min = right;
	}
	if (min != i) {
		swap(trains[i], trains[min]);
		siftDown(min);
	}
}

void heap::extractMin() {
	trains[0] = trains[size];
	size -= size;
	if (size != 0) {
		siftDown(0);
	}
}

int main() {
	int n = 0;
	cin >> n;
	train NewTrain;
	heap Station(2);
	cin >> NewTrain.arrivalTime >> NewTrain.departureTime;
	Station.add(NewTrain);
	for (int i = 1; i < n; ++i) {
		cin >> NewTrain.arrivalTime >> NewTrain.departureTime;
		if (!Station.isEmpty()) {
			if (Station.getMinimum().departureTime < NewTrain.arrivalTime) {
				Station.extractMin();
			}
			Station.add(NewTrain);
		}
	}
	cout << Station.k;
	return 0;
}
