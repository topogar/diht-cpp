/*Дана последовательность N прямоугольников различной ширины и высоты (wi, hi). Прямоугольники расположены, начиная с точки (0, 0), на оси ОХ вплотную друг за другом (вправо).
Требуется найти M - площадь максимального прямоугольника (параллельного осям координат), который можно вырезать из этой фигуры.

Время работы - O(n).

В первой строке задано число N (1 ≤ N ≤ 10000).
Далее идет N строк. В каждой строке содержится два числа width и height: ширина и высота i-го прямоугольника.
(0 < width ≤ 10000, 0 ≤ height ≤ 10000)*/

#include <iostream>
using namespace std;

struct rectangle
{
	int width = 0;
	int height = 0;
};

class stack {
public:
	int bufferSize;
	rectangle* buffer;
	int top;

	stack(int _bufferSize) :
		bufferSize(_bufferSize),
		top(-1)
	{
		buffer = new rectangle[bufferSize];
		buffer[0].height = -1;
	}

	~stack() {
		delete[] buffer;
	}

	rectangle pop();
	void push(rectangle _element);
	void doubleBuffer();
};

rectangle stack::pop() {
	return buffer[top];
}

void stack::push(rectangle _element) {
	if (top + 1 >= bufferSize) {
		doubleBuffer();
	}
	buffer[top + 1] = _element;
}

void stack::doubleBuffer() {
	int newBufferSize = bufferSize * 2;
	rectangle* newBuffer = new rectangle[newBufferSize];
	for (int i = 0; i < bufferSize; i++) {
		newBuffer[i] = buffer[i];
	}
	delete[] buffer;
	buffer = newBuffer;
	bufferSize = newBufferSize;
}

int main() {
	int m = 0;
	cin >> m;
	int s = 0;
	stack rectangles(2);
	rectangles.top = rectangles.top + 1;
	for (int i = 1; i < m + 2; i++) {
		int totalWidthOfPopRectangles = 0;
		rectangle element;
		if (i < m + 1) {
			cin >> element.width >> element.height;
		};
		while (element.height < rectangles.pop().height) {
			totalWidthOfPopRectangles = totalWidthOfPopRectangles + rectangles.pop().width;
			int mayBeS = totalWidthOfPopRectangles * rectangles.pop().height;
			if (mayBeS > s) {
				s = mayBeS;
			};
			rectangles.top = rectangles.top - 1;
		};
		element.width = element.width + totalWidthOfPopRectangles;

		rectangles.push(element);
		rectangles.top = rectangles.top + 1;
	}
	cout << s;
	return 0;
}
