/* Реализуйте структуру данных типа “множество строк” на основе динамической хеш-таблицы с открытой адресацией. Хранимые строки непустые и состоят из строчных латинских букв. Хеш-функция строки должна быть реализована с помощью вычисления значения многочлена методом Горнера. Начальный размер таблицы должен быть равным 8-ми. Перехеширование выполняйте при добавлении элементов в случае, когда коэффициент заполнения таблицы достигает 3/4. Структура данных должна поддерживать операции добавления строки в множество, удаления строки из множества и проверки принадлежности данной строки множеству. 1_1. Для разрешения коллизий используйте квадратичное пробирование. i-ая проба g(k, i)=g(k, i-1) + i (mod m). m - степень двойки. 1_2. Для разрешения коллизий используйте двойное хеширование.

Каждая строка входных данных задает одну операцию над множеством. Запись операции состоит из типа операции и следующей за ним через пробел строки, над которой проводится операция. Тип операции – один из трех символов: + означает добавление данной строки в множество; - означает удаление строки из множества; ? означает проверку принадлежности данной строки множеству. При добавлении элемента в множество НЕ ГАРАНТИРУЕТСЯ, что он отсутствует в этом множестве. При удалении элемента из множества НЕ ГАРАНТИРУЕТСЯ, что он присутствует в этом множестве. */

#include <iostream>
#include <vector>
#include <string>
using namespace std;

int Hash1(string str, int modulo) {
	int hash = 0;
	for (int i = 0; i < str.size(); i++) {
		hash = (hash * 19 + str[i]) % modulo;
	}
	return hash;
}

int Hash2(string str, int modulo) {
	int hash = 0;
	for (int i = 0; i < str.size(); i++) {
		hash = (hash * 23 + str[i]) % modulo;
	}
	return ((2 * hash + 1) % modulo);
}

class HashTableNode {
private:
	string key = "0";
	bool isDeleted = false;
public:
	HashTableNode() : key("0"), isDeleted(false) {}
	const bool IsDeleted() {
		return isDeleted;
	}
	void DeleteNode() {
		isDeleted = true;
	}
	void NotDeleteNode() {
		isDeleted = false;
	}
	const string GetKey() {
		return key;
	}
	void SetKey(string str) {
		key = str;
	}
};

class HashTable {
private:
	int bufferSize;
	int realSize;
	vector <HashTableNode> buffer;
	void ReHash() {
		int newBufferSize = bufferSize * 2;
		vector <HashTableNode> newBuffer(newBufferSize);
		for (int i = 0; i < bufferSize; i++) {
			if (buffer[i].GetKey() == "0" || buffer[i].IsDeleted()) {
				continue;
			}
			string str = buffer[i].GetKey();
			int hash = Hash1(str, newBufferSize);
			while (newBuffer[hash].GetKey() != "0") {
				hash = (hash + Hash2(str, newBufferSize)) % newBufferSize;;
			}
			newBuffer[hash].SetKey(str);
		}
		buffer = newBuffer;
		bufferSize = newBufferSize;
	}
public:
	HashTable() : buffer(8), bufferSize(8), realSize(0) {};
	string Insert(const string &str) {
		if (4 * (realSize + 1) >= bufferSize) {
			ReHash();
		}
		int hash = Hash1(str, bufferSize);
		int firstDeleted = -1;
		int count = 0;
		while (buffer[hash].GetKey() != "0" && count < bufferSize) {
			if (buffer[hash].GetKey() == str && !buffer[hash].IsDeleted()) {
				return "FAIL";
			}
			if (buffer[hash].IsDeleted() && firstDeleted < 0) {
				firstDeleted = hash;
			}
			hash = (hash + Hash2(str, bufferSize)) % bufferSize;
			count++;
		}
		if (firstDeleted >= 0) {
			buffer[firstDeleted].SetKey(str);
			buffer[firstDeleted].NotDeleteNode();
		}
		else {
			buffer[hash].SetKey(str);
		}
		realSize++;
		return "OK";
	}
	string Delete(const string &str) {
		int hash = Hash1(str, bufferSize);
		int count = 0;
		while (buffer[hash].GetKey() != "0" && count < bufferSize) {
			if (buffer[hash].GetKey() == str && !buffer[hash].IsDeleted()) {
				realSize--;
				buffer[hash].DeleteNode();
				return "OK";
			}
			count++;
			hash = (hash + Hash2(str, bufferSize)) % bufferSize;
		}
		return "FAIL";
	}

	string Search(const string &str) {
		int hash = Hash1(str, bufferSize);
		int count = 0;
		while (buffer[hash].GetKey() != "0" && count < bufferSize) {
			if (buffer[hash].GetKey() == str && !buffer[hash].IsDeleted()) {
				return "OK";
			}
			count++;
			hash = (hash + Hash2(str, bufferSize)) % bufferSize;
		}
		return "FAIL";
	}
};

int main() {
	HashTable table;
	char command;
	while (cin >> command) {
		string str;
		cin >> str;
		if (command == '?') {
			cout << table.Search(str);
		}
		if (command == '+') {
			cout << table.Insert(str);
		}
		if (command == '-') {
			cout << table.Delete(str);
		}
		cout << '\n';

	}
	return 0;
}
