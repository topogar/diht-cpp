/* В город N приехал цирк с командой атлетов. Они хотят удивить горожан города N — выстроить из своих тел башню максимальной высоты. Башня — это цепочка атлетов, первый стоит на земле, второй стоит у него на плечах, третий стоит на плечах у второго и т.д. Каждый атлет характеризуется силой si (kg) и массой mi (kg). Сила — это максимальная масса, которую атлет способен держать у себя на плечах. К сожалению ни один из атлетов не умеет программировать, так как всю жизнь они занимались физической подготовкой, и у них не было времени на изучение языков программирования. Помогите им, напишите программу, которая определит максимальную высоту башни, которую они могут составить. Известно, что если атлет тяжелее, то он и сильнее: если mi>mj, то si > sj. Атлеты равной массы могут иметь различную силу. */

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

struct athlete {
	int force = 0;
	int weight = 0;
	athlete(int force, int weight) : force(force), weight(weight) {};
};

bool comparator(athlete a, athlete b) {
	if (a.force < b.force) return true;
	if (a.force >= b.force) return false;
}

template<typename iterator>
int maxHeight(iterator begin, iterator end) {
	iterator i = begin;
	int height = 0;
	int weight = 0;
	while (i < end) {
		if ((*i).force >= weight) {
			weight = weight + (*i).weight;
			height++;
		}
		i = i + 1;
	}
	return height;
}

int main()
{
	vector <athlete> athletes;
	int weight = 0;
	int force = 0;
	while (cin >> weight) {
		cin >> force;
		athlete element(force, weight);
		athletes.push_back(element);
	};
	sort(athletes.begin(), athletes.end(), comparator);
	cout << maxHeight(athletes.begin(), athletes.end());
	return 0;
}
