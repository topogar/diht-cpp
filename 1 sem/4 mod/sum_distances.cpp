/* Дано невзвешенное дерево. Расстоянием между двумя вершинами будем называть количество ребер в пути, соединяющем эти две вершины. Для каждой вершины определите сумму расстояний до всех остальных вершин. Время работы должно быть O(n).

В первой строке записано количество вершин n ≤ 10000. Затем следует n - 1 строка, описывающая ребра дерева. Каждое ребро - это два различных целых числа - индексы вершин в диапазоне [0, n-1]. Индекс корня – 0. В каждом ребре родительской вершиной является та, чей номер меньше. */

#include <iostream>
#include <vector>
using namespace std;

struct tree {
	int number_sons = 0;
	int distance_to_sons = 0;
	int parent = 0;
	int distance = 0;
};

int main()
{
	int n = 0;
	cin >> n;
	vector <tree> trees(n);
	vector <vector <int>> tree1(n);
	for (int i = 0; i < n - 1; i++) {
		int x = 0;
		int y = 0;
		cin >> x;
		cin >> y;
		if (x < y) {
			tree1[x].push_back(y);
		}
		else {
			tree1[y].push_back(x);
		}
	}

	for (int i = n - 1; i >= 0; i--) {
		if (tree1[i].empty()) {
			trees[i].number_sons = 0;
			trees[i].distance_to_sons = 0;
		}
		else {
			int t = tree1[i].size();
			int sons = 0;
			int distance = 0;
			int k = 0;
			while (t > 0) {
				sons = sons + trees[tree1[i][k]].number_sons + 1;
				distance = distance + 1 + trees[tree1[i][k]].distance_to_sons + trees[tree1[i][k]].number_sons;
				trees[tree1[i][k]].parent = i;
				k++;
				t--;
			}
			trees[i].number_sons = sons;
			trees[i].distance_to_sons = distance;
		}
	}
	trees[0].distance = trees[0].distance_to_sons;
	cout << trees[0].distance << '\n';
	for (int i = 1; i < n; i++) {
		trees[i].distance = trees[trees[i].parent].distance + n - 2 * trees[i].number_sons - 2;
		cout << trees[i].distance << '\n';
	}
	return 0;
}
