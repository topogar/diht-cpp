/* Дано N кубиков. Требуется определить каким количеством способов можно выстроить из этих кубиков пирамиду. */

#include <iostream>
#include <vector>
using namespace std;

long long countamount(int i, int j, vector <vector<long long>> &c) {
	if (j > i) {
		return c[i][i];
	}
	if (j <= i && j <= i - j) return (c[i][j - 1] + c[i - j][j]);
	return (c[i][j - 1] + c[i - j][i - j]);
}

long long amount(int n) {
	vector <vector<long long>> pyramid(n + 1, vector<long long> (n + 1));
	pyramid[0][0] = 1;
	for (int i = 1; i <= n; i++) {
		pyramid[i][0] = 0;
		pyramid[0][i] = 1;
	}
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= i; j++) {
			pyramid[i][j] = countamount(i, j, pyramid);
		}
	}
	return pyramid[n][n];
}

int main()
{
	int n = 0;
	cin >> n;
	cout << amount(n);
    return 0;
}
