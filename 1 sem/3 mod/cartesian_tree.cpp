/* Дано число N < 10^6 и последовательность пар целых чисел из [-231, 231] длиной N. Построить декартово дерево из N узлов, характеризующихся парами чисел (Xi, Yi). Каждая пара чисел (Xi, Yi) определяет ключ Xi и приоритет Yi в декартовом дереве. Добавление узла в декартово дерево выполняйте второй версией алгоритма, рассказанного на лекции: При добавлении узла (x, y) выполняйте спуск по ключу до узла P с меньшим приоритетом. Затем разбейте найденное поддерево по ключу x так, чтобы в первом поддереве все ключи меньше x, а во втором больше или равны x. Получившиеся два дерева сделайте дочерними для нового узла (x, y). Новый узел вставьте на место узла P. Построить также наивное дерево поиска по ключам Xi. Равные ключи добавляйте в правое поддерево. Вычислить количество узлов в самом широком слое декартового дерева и количество узлов в самом широком слое наивного дерева поиска. Вывести их разницу. Разница может быть отрицательна. */

#include <iostream>
#include <queue>
using namespace std;

struct CBinaryNode {
	int Key;
	CBinaryNode *Left;
	CBinaryNode *Right;
};

struct CTreapNode {
	int Key;
	int Priority;
	CTreapNode *Left = nullptr;
	CTreapNode *Right = nullptr;
	CTreapNode(int key, int Priority) : Key(key), Priority(Priority){}
    
};
void Split (CTreapNode* СurrentNode, int Key, CTreapNode*& Left, CTreapNode*& Right){
	if (!СurrentNode) {
		Right = nullptr;
		Left = nullptr;
	}
	else if (СurrentNode->Key < Key) {
		Split(СurrentNode->Right, Key, СurrentNode->Right, Right);
		Left = СurrentNode;
	}
	else {
		Split(СurrentNode->Left, Key, Left, СurrentNode->Left);
		Right = СurrentNode;
	}
}

CTreapNode* Add(CTreapNode*& node, int key, int Priority)
{
	if (node == NULL) {
		node = new CTreapNode(key, Priority);
		return node;
	}
	else {
		if (node->Priority < Priority) {
			CTreapNode* Left;
			CTreapNode* Right;
			Split(node, key, Left, Right);
			node = new CTreapNode(key, Priority);
			node->Left = Left;
			node->Right = Right;
			return node;
		}
		if (key > node->Key) {
			Add(node->Right, key, Priority);
		}
		else {
			Add(node->Left, key, Priority);
		}
        return nullptr;
	}
}

void Insert(CBinaryNode*& Node, int Key) {
	if (Node == NULL) {
		Node = new CBinaryNode;
		Node->Key = Key;
		Node->Left = NULL;
		Node->Right = NULL;
		return;
	};
	if (Key > Node->Key) {
		Insert(Node->Right, Key);
	}
	else {
		Insert(Node->Left, Key);
	};
};

template<typename T>
int TraverseBFS(T root) {
	queue<pair<int, T>> q;
	pair<int, T> f = make_pair(0, root);
	q.push(f);
	int k = 0;
	int level = 0;
	int max = 0;
	while (!q.empty()) {
		while (!q.empty() && q.front().first == level) {
			T node = q.front().second;
			q.pop();
			k++;
			if (node->Left != nullptr) {
				f = make_pair(level + 1, node->Left);
				q.push(f);
			}
			if (node->Right != nullptr) {
				f = make_pair(level + 1, node->Right);
				q.push(f);
			}

		}
		level++;
		if (k > max) {
			max = k;
		}
		k = 0;
	}
	return max;
}

template <typename T>
void DeleteCNode(T Node) {
	if (Node == NULL) {
		return;
	};
	DeleteCNode(Node->Left);
	DeleteCNode(Node->Right);
	delete Node;
};

int main()
{
	int key = 0;
	int kek = 0;
	int n = 0;
	cin >> n;
	CBinaryNode* Node = NULL;
	CTreapNode* Nodet = NULL;
	for (int i = 0; i < n; i++) {
		cin >> key;
		cin >> kek;
		Insert(Node, key);
		Add(Nodet, key, kek);
	} 
	cout << TraverseBFS(Nodet) -TraverseBFS(Node);
	DeleteCNode(Node);
	DeleteCNode(Nodet);
	return 0;
}
