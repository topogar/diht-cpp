/* В одной военной части решили построить в одну шеренгу по росту. Т.к. часть была далеко не образцовая, то солдаты часто приходили не вовремя, а то их и вовсе приходилось выгонять из шеренги за плохо начищенные сапоги. Однако солдаты в процессе прихода и ухода должны были всегда быть выстроены по росту – сначала самые высокие, а в конце – самые низкие. За расстановку солдат отвечал прапорщик, который заметил интересную особенность – все солдаты в части разного роста. Ваша задача состоит в том, чтобы помочь прапорщику правильно расставлять солдат, а именно для каждого приходящего солдата указывать, перед каким солдатом в строе он должен становится. Требуемая скорость выполнения команды - O(log n).

Первая строка содержит число N – количество команд (1 ≤ N ≤ 90 000). В каждой следующей строке содержится описание команды: число 1 и X если солдат приходит в строй (X – рост солдата, натуральное число до 100 000 включительно) и число 2 и Y если солдата, стоящим в строе на месте Y надо удалить из строя. Солдаты в строе нумеруются с нуля. */

#include <iostream>
#include <queue>
#include <vector>
using namespace std;

struct Node {
	int Key;
	int Height = 1;
	int Size = 1;
	Node* Left = nullptr;
	Node* Right = nullptr;
	Node(int k) : Key(k) {};
};

int Height(Node* p) {
	return (p != nullptr) ? p->Height : 0;
}

int Size(Node* p) {
	return (p != nullptr) ? p->Size : 0;
}

int Bfactor(Node* p) {
	return Height(p->Right) - Height(p->Left);
}

void FixHeight(Node* node) {
	int LeftHeight = Height(node->Left);
	int RightHeight = Height(node->Right);
	node->Height = (LeftHeight > RightHeight ? LeftHeight : RightHeight) + 1;
}

void FixSize(Node* node) {
	node->Size = Size(node->Left) + Size(node->Right) + 1;
}


Node* RotateRight(Node* p)
{
	Node* q = p->Left;
	p->Left = q->Right;
	q->Right = p;
	FixHeight(p);
	FixHeight(q);
	FixSize(p);
	FixSize(q);
	return q;
}

Node* RotateLeft(Node* q)
{
	Node* p = q->Right;
	q->Right = p->Left;
	p->Left = q;
	FixHeight(q);
	FixHeight(p);
	FixSize(q);
	FixSize(p);
	return p;
}

Node* Balance(Node* p)
{
	FixHeight(p);
	FixSize(p);
	if (Bfactor(p) == 2) {
		if (Bfactor(p->Right) < 0) {
			p->Right = RotateRight(p->Right);
		}
		return RotateLeft(p);
	}
	if (Bfactor(p) == -2) {
		if (Bfactor(p->Left) > 0) {
			p->Left = RotateLeft(p->Left);
		}
		return RotateRight(p);
	}
	return p;
}

Node* insert(Node* p, int k)
{
	if (p == nullptr) {
		return new Node(k);
	}
	if (k < p->Key) {
		p->Left = insert(p->Left, k);
	}
	else {
		p->Right = insert(p->Right, k);
	}
	return Balance(p);
}

int Position(Node* p, int Key) {
	int k = 0;
	while (Key != p->Key) {
		if (Key > p->Key) {
			p = p->Right;
		}
		if (Key < p->Key) {
			k = k + Size(p->Right) + 1;
			p = p->Left;
		}
	}
	return k + Size(p->Right);
}

int Search(Node* p, int k) {
	Node* q = p;
	while (q) {
		if (Size(q->Right) == k) {
			return q->Key;
		}
		else if (Size(q->Right) > k) {
			q = q->Right;
		}
		else {
			k -= (Size(q->Right) + 1);
			q = q->Left;
		}
	}
	return -1;
}

Node* FindMin(Node* p)
{
	return p->Left ? FindMin(p->Left) : p;
}

Node* RemoveMin(Node* p)
{
	if (p->Left == 0) {
		return p->Right;
	}
	p->Left = RemoveMin(p->Left);
	return Balance(p);
}

void DeleteTree(Node* p) {
	if (p == nullptr) {
		return;
	}
	DeleteTree(p->Left);
	DeleteTree(p->Right);
	delete p;

}

Node* Remove(Node* p, int k)
{
	if (!p) {
		return 0;
	}
	if (k < p->Key) {
		p->Left = Remove(p->Left, k);
	}
	else if (k > p->Key) {
		p->Right = Remove(p->Right, k);
	}
	else {
		Node* q = p->Left;
		Node* r = p->Right;
		delete p;
		if (!r) {
			return q;
		}
		Node* min = FindMin(r);
		min->Right = RemoveMin(r);
		min->Left = q;
		return Balance(min);
	}
	return Balance(p);
}

int main()
{
	int n = 0;
	cin >> n;
	Node* p = nullptr;
	vector<int> c;
	for (int i = 0; i < n; i++) {
		int a = 0;
		cin >> a;
		if (a == 1) {
			int b = 0;
			cin >> b;
			p = insert(p, b);
			int t = Position(p, b);
			c.push_back(t);
		}
		if (a == 2) {
			int b = 0;
			cin >> b;
			b = Search(p, b);
			p = Remove(p, b);
		}
	}
	DeleteTree(p);
	for (int i = 0; i < c.size(); i++) {
		cout << c[i] << ' ';
	}
	return 0;
}

