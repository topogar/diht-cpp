/* Дано число N < 10^6 и последовательность целых чисел из [-231..231] длиной N. Требуется построить бинарное дерево, заданное наивным порядком вставки. Т.е., при добавлении очередного числа K в дерево с корнем root, если root→Key ≤ K, то узел K добавляется в правое поддерево root; иначе в левое поддерево root. Выведите элементы в порядке pre-order (сверху вниз).

Рекурсия запрещена. */

#include <iostream>
using namespace std;

struct CBinaryNode {
	int Key;
	CBinaryNode *Left;
	CBinaryNode *Right;
};

void TraverseDFS(CBinaryNode* Node) {
	if (Node == NULL) {
		return;
	};
	cout << Node->Key << ' ';
	TraverseDFS(Node->Left);
	TraverseDFS(Node->Right);
};

void Delete(CBinaryNode* Node) {
	if (Node == NULL) {
		return;
	};
	Delete(Node->Left);
	Delete(Node->Right);
	delete Node;
};

void Insert(CBinaryNode*& Node, int Key) {
	if (Node == NULL) {
		Node = new CBinaryNode;
		Node->Key = Key;
		Node->Left = nullptr;
		Node->Right = nullptr;
		return;
	};
	if (Key > Node->Key) {
		Insert(Node->Right, Key);
	}
	else {
		Insert(Node->Left, Key);
	};
}

int main()
{
	int key = 0;
	int n = 0;
	cin >> n;
	CBinaryNode* Node = nullptr;
	for (int i = 0; i < n; i++) {
		cin >> key;
		Insert(Node, key);
	};
	TraverseDFS(Node);
	Delete(Node);
	return 0;
}
