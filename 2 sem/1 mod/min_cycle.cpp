/* Дан невзвешенный неориентированный граф. Найдите цикл минимальной длины.

В первой строке вводятся два натуральных числа N и M, не превосходящих 10000. Далее идут M строк по 2 числа (от 0 до N-1) - индексы вершин между которыми есть ребро. */

#include <iostream>
#include <vector>
#include <queue>

//c
class CListGraph {
private:
    std::vector<std::vector<int> > graph;
    int d = -1;
public:
    CListGraph(int _verticesCount) : graph(_verticesCount) {};

    void AddEdge(int from, int to) {
        graph[from].push_back(to);
        graph[to].push_back(from);
    }

    int MainBfs() {
        for (int i = 0; i < graph.size(); i++) {
            if (d == 3) {
                break;
            }
            Bfs(i);
        }
        return d;
    }

    void Bfs(int start) {
        std::vector<int> distance(graph.size());
        std::vector<bool> mark(graph.size());
        std::vector<int> pred(graph.size());
        std::queue<int> q;
        q.push(start);
        distance[start] = 0;
        mark[start] = true;
        while (!q.empty()) {
            int v = q.front();
            q.pop();
            if (d == 3) break;
            for (int i = 0; i < graph[v].size(); i++) {
                if (graph[v][i] == pred[v]) continue;
                if (mark[graph[v][i]] == true) {
                    if (d == -1) {
                        d = distance[graph[v][i]] + distance[v] + 1;
                        if (d == 3) break;
                    }
                    if (distance[graph[v][i]] + distance[v] < d) {
                        d = distance[graph[v][i]] + distance[v] + 1;
                        if (d == 3) break;
                    }
                }
                if (mark[graph[v][i]] == false) {
                    distance[graph[v][i]] = distance[v] + 1;
                    mark[graph[v][i]] = true;
                    q.push(graph[v][i]);
                    pred[graph[v][i]] = v;
                }
            }
        }
    }
};

int main() {
    int n = 0, m = 0;
    std::cin >> n >> m;
    CListGraph graph(n);
    for (int i = 0; i < m; i++) {
        int a = 0, b = 0;
        scanf("%d", &a);
        scanf("%d", &b);
        graph.AddEdge(a, b);
    }
    std::cout << graph.MainBfs();
    return 0;
}
