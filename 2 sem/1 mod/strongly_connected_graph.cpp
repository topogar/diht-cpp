/* Дан ориентированный граф. Определите, какое минимальное число ребер необходимо добавить, чтобы граф стал сильносвязным. В графе возможны петли.

В первой строке указывается число вершин графа V, во второй – число ребер E, в последующих – E пар вершин, задающих ребра. */

#include <iostream>
#include <vector>

class CListGraph {
private:
    std::vector<std::vector<int> > graph;
    std::vector<std::vector<int> > invGraph;
    std::vector<int> order;
    std::vector<bool> isVisited;
    std::vector<int> component;
    int numC;
    std::vector<int> comp;
public:
    CListGraph(int _verticesCount)
            : graph(_verticesCount), invGraph(_verticesCount), isVisited(_verticesCount, false), numC(1), comp(_verticesCount, 0) {};

    void AddEdge(int from, int to) {
        graph[from].push_back(to);
        invGraph[to].push_back(from);
    }
    void dfs1 (int v) {
        isVisited[v] = true;
        for (int i = 0; i < graph[v].size(); i++) {
            if (!isVisited[graph[v][i]]) {
                dfs1 (graph[v][i]);
            }
        }
        order.push_back(v);
    }
    void dfs2 (int v) {
        isVisited[v] = true;
        component.push_back(v);
        for (int i = 0; i < invGraph[v].size(); i++) {
            if (!isVisited[invGraph[v][i]]) {
                dfs2 (invGraph[v][i]);
            }
        }
    }
    void mainDfs(int v) {
        for (int i = 0; i < graph.size(); i++) {
            if (!isVisited[i]) {
                dfs1(i);
            }
        }
        isVisited.assign(graph.size(), false);
        for (int i = 0; i < graph.size(); i++) {
            int v = order[graph.size() - i -1];
            if (!isVisited[v]) {
                dfs2(v);
                for (int i = 0; i < component.size(); i++) {
                    comp[component[i]] = numC;
                }
                numC++;
                component.clear();
            }
        }
    }
    int isAndSt(std::vector<std::vector<bool> > newG) {
        int k = 0;
        int st = 0;
        for (int i = 0; i < newG.size(); i++) {
            for (int j = 0; j < newG.size(); j++) {
                if (newG[i][j] == 1 && i != j) {
                    k = 1;
                    break;
                }
            }
            if (k == 0) {
                st++;
            }
            k = 0;
        }
        int ist = 0;
        for (int i = 0; i < newG.size(); i++) {
            for (int j = 0; j < newG.size(); j++) {
                if (newG[j][i] == 1 && i != j) {
                    k = 1;
                    break;
                }
            }
            if (k == 0) {
                ist++;
            }
            k = 0;
        }
        if (st > ist) {
            ist = st;
        }
        return ist;
    }
    int number() {
        if (numC == 2) {
            return 0;
        }
        std::vector< std::vector<bool> > newG(numC - 1, std::vector<bool>(numC - 1, false));
        for (int i = 0; i < numC - 1; i++) {
            for (int j = 0; j < graph.size(); j++) {
                if (comp[j] == i + 1) {
                    for (int k = 0; k < graph[j].size(); k++) {
                        newG[i][comp[graph[j][k]] - 1] = true;
                    }
                }
            }
        }
        return isAndSt(newG);
    }
};

int main() {
    int n = 0, m = 0, a = 0, b = 0;
    std::cin >> n >> m;
    CListGraph graph(n);
    for (int i = 0; i < m; i++) {
        std::cin >> a >> b;
        graph.AddEdge(a - 1, b - 1);
    }
    graph.mainDfs(0);
    std::cout <<  graph.number();
    return 0;
}
