/* Единственный способ попасть в Зал Круглых Столов – пройти через Колонный Коридор. 
Стены Коридора изображаются на карте прямыми линиями, которые параллельны осиOY системы координат. 
Вход в Коридор находится снизу, а выход из Коридора в Зал – сверху. 
В Коридоре есть цилиндрические (на карте круглые) Колонны одинакового радиуса R.
Разработайте алгоритм, который по информации о размерах Коридора, и размещения Колонн определяет диаметр наибольшего из Круглых Столов, который можно пронести через такой Коридор, сохраняя поверхность Стола горизонтальной.

В первой строке задано два целых числа XL и XR - x-координаты левой и правой стен Коридора. 
Во второй строке находится целое число R - радиус всех Колон. 
В третьей - целое число N , задающее количество Колон. 
Далее идут N строк, в каждой из которых по два целых числа – x- и y-координаты центра соответствующей Колоны.
Все входные координаты – целые числа, не превышающие по модулю 1000000. */

#include <iostream>
#include <vector>
#include <cmath>
#include <queue>
#include <iomanip>
struct location {
    int x = 0;
    int y = 0;
    int r = 0;
};

class CListGraph {
private:
    std::vector<std::vector<int> > graph;
    std::vector<location> xyr;
    int rad_;
public:
    CListGraph(int _verticesCount, int r) : graph(_verticesCount + 2), rad_(r) {};

    void AddEdge(int from, int to) {
        graph[from].push_back(to);
        graph[to].push_back(from);
    }

    void AddLocation(int x, int y, int r) {
        location z;
        z.x = x;
        z.y = y;
        z.r = r;
        xyr.push_back(z);
    }

    bool Bfs(int start) {
        std::vector<bool> mark(graph.size());
        std::queue<int> q;
        q.push(start);
        mark[start] = true;
        while (!q.empty()) {
            int v = q.front();
            q.pop();
            for (int i = 0; i < graph[v].size(); i++) {
                if (mark[graph[v][i]] == false) {
                    mark[graph[v][i]] = true;
                    q.push(graph[v][i]);
                }
                if (graph[v][i] == 1) {
                    return false;
                }
            }
        }
        return true;
    }
    bool can(double rad) {
        for (int i = 0; i < graph.size(); i++) {
            graph[i].clear();
       }
        for (int i = 0; i < 2; i++) {
            for (int j = i + 1; j < xyr.size(); j++) {
                if (abs(xyr[i].x - xyr[j].x) - xyr[j].r < rad) {
                    AddEdge(i, j);
                }
            }
        }
        for (int i = 2; i < xyr.size() - 1; i++) {
            for (int j = i + 1;j < xyr.size(); j++) {
                if (sqrt(pow(xyr[i].x - xyr[j].x, 2) + pow(xyr[i].y - xyr[j].y, 2)) - 2*rad_ < rad) {
                    AddEdge(i, j);
                }
            }
        }
        return Bfs(0);
    }
    double s() {
        double first = 0;
        double last = xyr[1].x - xyr[0].x;
        for (int i = 0; i < 35; i++) {
            double mid = (last + first) / 2;
            if (can(mid)) {
                first = mid;
            } else {
                last = mid;
            }
        }
        return first;
    }
};

int main() {
    int a = 0, b = 0, r = 0, n = 0;
    std::cin >> a >> b >> r >> n;
    CListGraph graph(n, r);
    graph.AddLocation(a, 0, 0);
    graph.AddLocation(b, 0, 0);
    for (int i = 0; i < n; i++) {
        std::cin >> a >> b;
        graph.AddLocation(a, b, r);
    }
    double t = graph.s();
    std::cout << std::fixed << std::setprecision(3) << t << std::endl;
    return 0;
}
