/* Дана матрица смежности ориентированного графа. Проверьте, содержит ли граф вершину-сток. То есть вершину, в которую ведут ребра из всех вершин, и из которой не выходит ни одного ребра. Требуемая сложность O(V).

В 1-й строке вводится кол-во вершин графа, далее матрица смежности. Числа в строке матрицы разделены пробелом. */

#include <iostream>
#include <vector>

class CMatrixGraph {
private:
    std::vector <std::vector<bool> > graph;
    std::vector <std::pair<int, int> > number;
public:
    CMatrixGraph(int _verticesCount) : graph(_verticesCount), number(_verticesCount, std::make_pair(0, 0)) {}
    void AddEdge(bool i, int posv, int posg) {
        graph[posv].push_back(i);
        if (i == 1) {
            number[posv].first++;
            number[posg].second++;
        }
    }
    bool HaveStok() {
        for (int i = 0; i < graph.size(); i++) {
            if (number[i].first == 0&& number[i].second == graph.size() - 1) {
                return true;
            }
        }
        return false;
    }
};

int main() {
    int n = 0;
    std::cin >> n;
    CMatrixGraph graph(n);
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            bool m = 0;
            std::cin >> m;
            graph.AddEdge(m, i, j);
        }
    }
    if (graph.HaveStok()) {
        std::cout << "YES";
    } else {
        std::cout << "NO";
    }
    return 0;
}
