/* Дан невзвешенный неориентированный граф. В графе может быть несколько кратчайших путей между какими-то вершинами. Найдите количество различных кратчайших путей между заданными вершинами. Требуемая сложность O(V+E).

v: кол-во вершин (макс. 50000),
n: кол-во ребер(макс. 200000),
n пар реберных вершин,
пара вершин (u, w) для запроса. */

#include <iostream>
#include <vector>
#include <queue>

class CListGraph {
private:
    std::vector<std::vector<int> > graph;
public:
    CListGraph(int _verticesCount) : graph(_verticesCount) {};

    void AddEdge(int from, int to) {
        graph[from].push_back(to);
        graph[to].push_back(from);
    }

    int Bfs(int start, int end) {
        std::vector<int> distance(graph.size());
        std::vector<bool> mark(graph.size());
        std::vector<int> number(graph.size(), 0);
        std::queue<int> q;
        int minDist = -1;
        q.push(start);
        distance[start] = 0;
        mark[start] = true;
        number[start] = 1;
        while (!q.empty()) {
            int v = q.front();
            if (distance[v] >= minDist && minDist != -1) break;
            q.pop();
            for (int i = 0; i < graph[v].size(); i++) {
                if (mark[graph[v][i]] == true && distance[v] == distance[graph[v][i]] - 1) {
                    number[graph[v][i]] += number[v];
                }
                if (mark[graph[v][i]] == false) {
                    distance[graph[v][i]] = distance[v] + 1;
                    number[graph[v][i]] += number[v];
                    mark[graph[v][i]] = true;
                    q.push(graph[v][i]);
                }
                if (graph[v][i] == end) {
                   minDist = distance[graph[v][i]];
                   break;
                }
            }
        }
        return number[end];
    }
};

int main() {
    int n = 0, m = 0;
    std::cin >> n >> m;
    CListGraph graph(n);
    for (int i = 0; i < m; i++) {
        int a = 0, b = 0;
        scanf("%d", &a);
        scanf("%d", &b);
        graph.AddEdge(a, b);
    }
    int begin = 0, end = 0;
    scanf("%d", &begin);
    scanf("%d", &end);
    std::cout << graph.Bfs(begin, end);
    return 0;
}
