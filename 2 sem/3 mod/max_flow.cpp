/* Задан ориентированный граф, каждое ребро которого обладает целочисленной пропускной способностью. Найдите максимальный поток из вершины с номером 1 в вершину с номером n


Первая строка входного файла содержит n и m — количество вершин и количество ребер графа.
Следующие m строк содержат по три числа: номера вершин, которые соединяет соответствующее ребро графа и его пропускную способность. Пропускные способности не превосходят 105. */

#include <iostream>
#include <queue>
#include <vector>
using namespace std;

class Graph {
private:
    vector<vector<int>> graph;
public:
    Graph(int n) : graph(n, vector<int>(n, 0)) {}
    void AddEdge(int to, int from, int capacity) {

        graph[to][from] += capacity;
        //graph[from][to] += capacity;
    }
    vector<vector<int>> RetGraph() {
        return graph;
    }
};

bool Bfs(int start, int end, vector<int>& pred, vector<vector<int> > graph) {
    vector<int> distance(graph.size());
    vector<bool> mark(graph.size(), false);
    queue<int> q;
    q.push(start);
    distance[start] = 0;
    mark[start] = true;

    while (!q.empty()) {
        int v = q.front();
        q.pop();
        for (int i = 0; i < graph.size(); i++) {
            if (mark[i] == false && graph[v][i] != 0) {
                distance[i] = distance[v] + 1;
                mark[i] = true;
                q.push(i);
                pred[i] = v;
            }

            if (i == end && graph[v][i] != 0) return true;
        }
    }
    return false;
}

int EdmKarp(vector<vector<int> > graph) {
    vector<vector<int> > flow_graph(graph.size(), vector<int> (graph.size(), 0));
    vector<vector<int> > os_graph = graph;
    vector<int> pred(graph.size(), -1);
    int k = 0;
    while (Bfs(0, graph.size() - 1, pred, os_graph)) {

        int s = 1000000000;
        int from = pred[graph.size() - 1];
        int to = graph.size() - 1;
        while (true) {
            s = min(s, os_graph[from][to]);
            if (from == 0) break;
            to = from;
            from = pred[to];
        }
        k += s;
        for (int i = graph.size() - 1; i != 0; i = pred[i]) {
            int from = pred[i];
            int to = i;
            flow_graph[from][to] += s;
            flow_graph[to][from] = - flow_graph[from][to];
            os_graph[from][to] = graph[from][to] - flow_graph[from][to];
            os_graph[to][from] = graph[to][from] - flow_graph[to][from];
        }
    }
    return k;
}

int main() {
    int n = 0, m = 0;
    cin >> n >> m;
    Graph graph(n);
    for (int i = 0; i < m; i++) {
        int from = 0, to = 0, capacity = 0;
        cin >> from >> to >> capacity;
        graph.AddEdge(from - 1, to - 1, capacity);
    }
    cout << EdmKarp(graph.RetGraph());
    return 0;
}
