/* Дан неориентированный связный граф. Требуется найти вес минимального остовного дерева в этом графе. 
Вариант 1. С помощью алгоритма Прима.
Вариант 2. С помощью алгоритма Крускала.
Вариант 3. С помощью алгоритма Борувки.

Первая строка содержит два натуральных числа n и m — количество вершин и ребер графа соответственно (1 ≤ n ≤ 20000, 0 ≤ m ≤ 100000). 
Следующие m строк содержат описание ребер по одному на строке. 
Ребро номер i описывается тремя натуральными числами bi, ei и wi — номера концов ребра и его вес соответственно (1 ≤ bi, ei ≤ n, 0 ≤ wi ≤ 100000). */

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Graph {
private:
    vector <pair< int, pair<int, int> > > edges;
public:
    void AddEdge(pair<int, pair<int, int>> edge) {
        edges.push_back(edge);
    }
    vector <pair<int, pair<int, int> > > GetEdges() {
        return edges;
    };
};

class DSU {
    vector<int> id;
    vector<int> size;
public:
    DSU(int n) {
        for (int i = 0; i < n; i++) {
            id.push_back(i);
            size.push_back(1);
        }
    };
    int find(int x) {
        if (id[x] == x)
            return x;
        return id[x] = find(id[x]);
    };
    void merge(int x, int y) {
        int rx = find(x), ry = find(y);
        if (rx == ry)
            return;
        if (size[rx] < size[ry]) {
            id[rx] = ry;
            size[ry] += size[rx];
        } else {
            id[ry] = rx;
            size[ry] += size[rx];
        }
    };
};

int Kruskala(int n, vector<pair<int, pair<int, int> > > edges) {
    vector<int> tree_id(n);
    DSU dsu(n);
    sort(edges.begin(), edges.end());
    int cost = 0;
    for (int i = 0; i < edges.size(); i++) {
        int from = edges[i].second.first;
        int to = edges[i].second.second;
        if (dsu.find(from) != dsu.find(to)) {
            cost += edges[i].first;
            dsu.merge(from, to);
        }
    }
    return cost;
}

int main() {
    int n = 0, m = 0;
    cin >> n >> m;
    Graph graph;
    for (int i = 0; i < m; i++) {
        int from = 0, to = 0, weight = 0;
        cin >> from >> to >> weight;
            graph.AddEdge(make_pair(weight, make_pair(from - 1, to - 1)));
    }
    cout << Kruskala(n, graph.GetEdges());
    return 0;
}
