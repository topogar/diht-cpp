#include <iostream>
#include <vector>
#include <array>
#include <string>
#include <cmath>
#include <queue>
#include <set>
using namespace std;

struct Argument {
    int pos = 0;
    string way = "";
    int dist = 0;
    Argument(int pos_, string way_, int dist_) {
        pos = pos_;
        way = way_;
        dist = dist_;
    }
};
int binpow (int number, int n) {
    if (n == 0)
        return 1;
    if (n % 2 == 1)
        return binpow (number, n-1) * number;
    else {
        int b = binpow (number, n/2);
        return b * b;
    }
}

class Vertex {
private:
    array<int, 9> position;
    int distance;
    string way;
public:
    Vertex(Argument arg) {
        for (int i = 1; i < 10; i++) {
            position[i - 1] = arg.pos / binpow(10, 9 - i) % 10;
        }
        way = arg.way;
        distance = arg.dist;
    }

    void show() {
        for (int i = 0; i < 9; i++) {
            cout << position[i];
        }
    }

    int FindNull() {
        for (int i = 0; i < position.size(); i++) {
            if (position[i] == 0) {
                return i;
            }
        }
    }

    int RetHash(array<int, 9> position_) {
        int tmp = 0;
        for (int i = 0; i < position_.size(); i++) {
            tmp = tmp + position_[i] * binpow(10, position_.size() - i - 1);
        }
        return tmp; // а лучше храни хеш
    }
    Argument GiveInfo() {
        return Argument(RetHash(position), way, distance);
    }
    int CreatePos(Vertex prevPos, int pos, string i) {
        if (i == "U") {
            swap(prevPos.position[pos], prevPos.position[pos - 3]);
        }
        if (i == "D") {
            swap(prevPos.position[pos], prevPos.position[pos + 3]);
        }
        if (i == "R") {
            swap(prevPos.position[pos], prevPos.position[pos + 1]);
        }
        if (i == "L") {
            swap(prevPos.position[pos], prevPos.position[pos - 1]);
        }
        return RetHash(prevPos.position);
    }
};

Argument bfs(int beg_pos) {
    queue<Argument> q;
    set<int> mark;
    q.push(Argument(beg_pos, "", 0));
    mark.insert(beg_pos);
    while (!q.empty()) {
        Vertex vert(q.front());
        q.pop();
        if (vert.GiveInfo().pos == 123456780) {
            return vert.GiveInfo();
        }
        int tmp = vert.FindNull();
        Argument arg = vert.GiveInfo();
        if (tmp > 2) {
            int hash = vert.CreatePos(vert, tmp, "U");
            if (mark.find(hash) == mark.end()) {
                mark.insert(hash);
                q.push(Argument(hash, arg.way + "U", arg.dist + 1));
            }
        }
        if (tmp < 6) {
            int hash = vert.CreatePos(vert, tmp, "D");
            if (mark.find(hash) == mark.end()) {
                mark.insert(hash);
                q.push(Argument(hash, arg.way + "D", arg.dist + 1));
            }
        }
        if ((tmp + 1) % 3 != 0) {
            int hash = vert.CreatePos(vert, tmp, "R");
            if (mark.find(hash) == mark.end()) {
                mark.insert(hash);
                q.push(Argument(hash, arg.way + "R", arg.dist + 1));
            }
        }
        if (tmp % 3 != 0) {
            int hash = vert.CreatePos(vert, tmp, "L");
            if (mark.find(hash) == mark.end()) {
                mark.insert(hash);
                q.push(Argument(hash, arg.way + "L", arg.dist + 1));
            }
        }
    }
    return Argument(-1, "", 0);
}

int main() {
    int n = 0;
    for (int i = 0; i < 3; i++) {
        int t = 0;
        int k = 0;
        int j = 0;
        cin >> t >> k >> j;
        n = n + t * binpow(10, 8 - 3 * i) + k * binpow(10, 7 - 3 * i) + j * binpow(10, 6 - 3 * i);
    }
    Argument t = bfs(n);
    if (t.pos == -1) {
        cout << -1;
    } else {
        cout << t.dist << "\n";
        cout << t.way;
    }

    return 0;
}
