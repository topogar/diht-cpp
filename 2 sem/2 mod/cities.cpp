/* Требуется отыскать самый короткий маршрут между городами. Из города может выходить дорога, которая возвращается в этот же город.

Первая строка содержит число N – количество городов.

Вторая строка содержит число M - количество дорог.

Каждая следующая строка содержит описание дороги (откуда, куда, время в пути). Все указанные дороги двусторонние. Между любыми двумя городами может быть больше одной дороги.

Последняя строка содержит маршрут (откуда и куда нужно доехать)

Требуемое время работы O((N + M)log N), где N – количество городов, M – известных дорог между ними.
N ≤ 10000, M ≤ 250000.
Длина каждой дороги ≤ 10000. */

#include <iostream>
#include <vector>
using namespace std;

struct Way {
    int distance;
    int to;
    Way(int distance_, int to_) {
        distance = distance_;
        to = to_;
    }
};

class CListGraph {
private:
    vector <vector<Way> > graph;
public:
    CListGraph(int _verticesCount) : graph(_verticesCount) {}
    void AddEdge(int from, int to, int distance) {
        graph[from].push_back(Way(distance, to));
        graph[to].push_back(Way(distance, from));
    }
    int dijkstra(int from, int to) {
        vector<int> dist(graph.size(), 100000000);
        dist[from] = 0;
        vector<bool> used(graph.size(), false);
        for (int j = 0; j < graph.size(); j++) {
            int v = -1;
            for (int i = 0; i < graph.size(); i++) {
                if (!used[i] && ((v == -1) || dist[i] < dist[v])) {
                    v = i;
                }
            }
            if (dist[v] == 100000000) break;
            used[v] = true;
            for (Way e : graph[v]) {
                if (dist[v] + e.distance < dist[e.to]) {
                    dist[e.to] = dist[v] + e.distance;
                }
            }
            if (v == to) {
                break;
            }
        }
        return dist[to];
    }
};

int main() {
    int n = 0, m = 0;
    cin >> n;
    cin >> m;
    CListGraph graph(n);
    for (int i = 0; i < m; i++) {
        int from = 0;
        int to = 0;
        int dist = 0;
        cin >> from >> to >> dist;
        graph.AddEdge(from, to, dist);
    }
    int a = 0, b = 0;
    cin >> a >> b;
    cout << graph.dijkstra(a, b);
    return 0;
}
