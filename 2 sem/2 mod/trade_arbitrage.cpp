/* Арбитраж - это торговля по цепочке различных валют в надежде заработать на небольших различиях в коэффициентах. Например, есть следующие курсы валют:
GBP/USD: 0.67
RUB/GBP: 78.66
USD/RUB: 0.02
Имея 1$ и совершив цикл USD->GBP->RUB->USD, получим 1.054$. Таким образом заработав 5.4

Первая строка содержит число N – количество возможных валют (определяет размер таблицы котировок). 
Далее следует построчное представление таблицы. Диагональные элементы (i, i) пропущены (подразумевается, что курс валюты к себе же 1.0). 
В элементе таблицы (i, j) содержится обменный курс i->j. 
Если обмен в данном направлении не производится, то -1. */

#include <iostream>
#include <vector>
#include <math.h>
using namespace std;

struct Edge {
    int from = 0;
    int to = 0;
    double distance = 0;
    Edge(int from_, int to_, double distance_) {
        from = from_;
        to = to_;
        distance = distance_;
    }
};

class Graph {
private:
    vector <Edge> Edges;
public:
    void AddEdge(Edge edge) {
        Edges.push_back(edge);
    }
    string Arbitrage(int vertices_count) {
        //Находим цикл отрицательный с помощью Форда-Беллмана
        vector<double> dist(vertices_count, 0);
        for (int i = 0; i < vertices_count; i++) {
            for (int j = 0; j < Edges.size(); j++) {
                if (dist[Edges[j].to] > dist[Edges[j].from] + Edges[j].distance) {
                    dist[Edges[j].to] = dist[Edges[j].from] + Edges[j].distance;
                    if (i == vertices_count - 1) {
                        return "YES";
                    }
                }
            }
        }
        return "NO";
    }
};
int main() {
    Graph graph;
    int n = 0;
    cin >> n;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            if (i != j) {
                double exchange_rate = 0;
                cin >> exchange_rate;
                if (exchange_rate != - 1) {
                    graph.AddEdge(Edge(i, j, -log(exchange_rate))); //- т.к. цикл положительный на самом деле ищем
                }
            }
        }
    }
    cout << graph.Arbitrage(n);
    return 0;
}

