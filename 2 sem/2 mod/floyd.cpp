/* Полный ориентированный взвешенный граф задан матрицей смежности. Постройте матрицу кратчайших путей между его вершинами. Гарантируется, что в графе нет циклов отрицательного веса.

В первой строке вводится единственное число N (1 < N < 100) — количество вершин графа. В следующих N строках по N чисел задается матрица смежности графа (j-ое число в i-ой строке — вес ребра из вершины i в вершину j). Все числа по модулю не превышают 100. На главной диагонали матрицы — всегда нули. */

#include <iostream>
#include <vector>
using namespace std;

class Graph {
private:
    vector< vector<int> > graph;
public:
    Graph(int vertices_count) : graph(vertices_count, vector<int>(vertices_count, 0)) {}
    void AddEdge(int from, int to, int distance) {
        graph[from][to] = distance;
    }
    void Floyd() {
        for (int i = 0; i < graph.size(); i++) {
            for (int j = 0; j < graph.size(); j++) {
                for (int k = 0; k < graph.size(); k++) {
                    graph[j][k] = min(graph[j][k], graph[j][i] + graph[i][k]);
                }
            }
        }
    }
    void info() {
        for (int i = 0; i < graph.size(); i++) {
            for (int j = 0; j < graph.size(); j++) {
                cout << graph[i][j] << " ";
            }
            cout << "\n";
        }
    }
};

int main() {
    int n;
    cin >> n;
    Graph graph(n);
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            int dist = 0;
            cin >> dist;
            graph.AddEdge(i, j, dist);
        }
    }
    graph.Floyd();
    graph.info();
    return 0;
}
