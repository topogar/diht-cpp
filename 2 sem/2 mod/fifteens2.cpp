/* Написать алгоритм для решения игры в “пятнашки”. Решением задачи является приведение к виду: [ 1 2 3 4 ] [ 5 6 7 8 ] [ 9 10 11 12] [ 13 14 15 0 ], где 0 задает пустую ячейку. Необходимо найти минимальное решение.

Если вам удалось найти решение, то в первой строке файла выведите число перемещений, которое требуется сделать в вашем решении. А во второй строке выведите соответствующую последовательность ходов: L означает, что в результате перемещения костяшки пустая ячейка сдвинулась влево, R – вправо, U – вверх, D – вниз. Если же выигрышная конфигурация недостижима, то выведите в выходной файл одно число −1. */

#include <iostream>
#include <vector>
#include <array>
#include <string>
#include <cmath>
#include <queue>
#include <set>
#include <map>
#include <unordered_map>
using namespace std;

struct Argument {
    unsigned long long pos = 0;
    string way = "";
    int dist = 0;
    int mayBeDist = 0;
    Argument(unsigned long long pos_, string way_, int dist_, int mayBeDist_) {
        pos = pos_;
        way = way_;
        dist = dist_;
        mayBeDist = mayBeDist_;
    }
};

class Vertex {
public:
    array<int, 16> position;
    int distance;
    string way;
    int mayBeDist;
    Vertex(unsigned long long pos, string way_, int dist) {
        for (int i = 0; i < 16; i++) {
            position[15 - i] = pos & 15;
            pos = pos >> 4;
        }
        way = way_;
        distance = dist;
        mayBeDist = 0;
        for (int i = 0; i < 16; i++) {
            if (position[i] != 0) {
                mayBeDist = mayBeDist + abs((position[i] - 1) / 4 - (i / 4)) + abs((position[i] - 1) % 4 - (i % 4));
            }
        }
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (position[j + 4* i] != 0 && (position[j + 4*i] - 1) / 4 - (j + 4*i) / 4 == 0) {
                    for (int k = j; k < 4; k++) {
                        if(position[k + 4* i] != 0 && (position[k + 4 * i] < position[j + 4*i]) && (position[k + 4*i] - 1) / 4 - (k + 4*i) / 4 == 0) {
                            mayBeDist = mayBeDist + 2;
                        }
                    }
                }
            }
        }
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (position[j * 4 + i] != 0 && (position[j * 4 + i] - 1) % 4 - (j * 4 + i) % 4 == 0) {
                    for (int k = j; k < 4; k++) {
                        if(position[k * 4 + i] != 0 && (position[k * 4 +  i] < position[j * 4 + i]) && (position[k * 4 + i] - 1) % 4 - (k * 4 + i) % 4 == 0) {
                            mayBeDist = mayBeDist + 2;
                        }
                    }
                }
            }
        }
        if (position[14] != 15 || position[11] != 12) {
            mayBeDist = mayBeDist + 2;
        }
    }

    void show() {
        for (int i = 0; i < 9; i++) {
            cout << position[i];
        }
    }

    int FindNull() {
        for (int i = 0; i < position.size(); i++) {
            if (position[i] == 0) {
                return i;
            }
        }
    }

    unsigned long long RetHash(array<int, 16> position_) {
        unsigned long long tmp = 0;
        for (int i = 0; i < position_.size(); i++) {
            tmp = (tmp << 4) + position_[i];
        }
        return tmp; // а лучше храни хеш
    }
    Argument GiveInfo() {
        return Argument(RetHash(position), way, distance, mayBeDist);
    }

    string GiveWay() {
        return way;
    }

    int GiveDist() {
        return distance;
    }

    unsigned long long CreatePos(Vertex prevPos, int pos, string i) {
        if (i == "U") {
            swap(prevPos.position[pos], prevPos.position[pos - 4]);
        }
        if (i == "D") {
            swap(prevPos.position[pos], prevPos.position[pos + 4]);
        }
        if (i == "R") {
            swap(prevPos.position[pos], prevPos.position[pos + 1]);
        }
        if (i == "L") {
            swap(prevPos.position[pos], prevPos.position[pos - 1]);
        }
        return RetHash(prevPos.position);
    }
};

const bool operator< (const Vertex &arg1, const Vertex &arg2) {
    if (arg1.mayBeDist + 0.865 * arg1.distance < arg2.mayBeDist + 0.865 * arg2.distance) {
        return false;
    }
    return true;
}

bool CanDo(Vertex vert) {
    array<int, 16> arg = vert.position;
    int k = 0;
    for (int i = 0; i < 16; i++) {
        if (arg[i] == 0) {
            k = k + i / 4 + 1;
        } else {
            int l = 0;
            for (int j = i; j < 16; j++) {
                if (arg[i] > arg[j] && arg[j] != 0) {
                    l++;
                }
            }
            k = k + l;
        }
    }
    if (k % 2 == 0) {
        return true;
    }
    return false;
}
void bfs(unsigned long long beg_pos) {
    priority_queue<Vertex> q;
    unordered_map<unsigned long long, string> mark;
    Vertex ve(beg_pos, "", 0);
    if (!CanDo(ve)) {
        cout << -1;
        return;
    }
    q.push(ve);
    mark.insert({beg_pos, ""});
    int t = 80;
    string k = "";
    Argument a(-1, "", -1, -1);
    while (!q.empty()) {
        Vertex vert = q.top();
        q.pop();
        if (vert.GiveInfo().pos == 1311768467463790320) {
            if (vert.distance < t) {
                t = vert.distance;
                k = vert.way;
                a.way = k;
                a.dist = t;
                if (vert.GiveInfo().dist == ve.mayBeDist) {
                    cout << vert.distance << "\n";
                    cout << vert.way;
                    return;
                }
            }
        }
        if (vert.distance + vert.mayBeDist > t) continue;
        int tmp = vert.FindNull();
        if (tmp > 3) {
            unsigned long long hash = vert.CreatePos(vert, tmp, "U");
            if (mark.find(hash) == mark.end() || mark[hash].length() > vert.distance) {
                mark.erase(hash);
                mark.insert(make_pair(hash, vert.GiveWay()));
                q.push(Vertex(hash, vert.GiveWay() + "D", vert.GiveDist() + 1));
            }
        }
        if (tmp < 12) {
            unsigned long long hash = vert.CreatePos(vert, tmp, "D");
            if (mark.find(hash) == mark.end() || mark[hash].length() > vert.distance) {
                mark.erase(hash);
                mark.insert(make_pair(hash, vert.GiveWay()));
                q.push(Vertex(hash, vert.GiveWay() + "U", vert.GiveDist() + 1));
            }
        }
        if ((tmp + 1) % 4 != 0) {
            unsigned long long hash = vert.CreatePos(vert, tmp, "R");
            if (mark.find(hash) == mark.end() || mark[hash].length() > vert.distance) {
                mark.erase(hash);
                mark.insert(make_pair(hash, vert.GiveWay()));
                q.push(Vertex(hash, vert.GiveWay() + "L", vert.GiveDist() + 1));
            }
        }
        if (tmp % 4 != 0) {
            unsigned long long hash = vert.CreatePos(vert, tmp, "L");
            if (mark.find(hash) == mark.end() || mark[hash].length() > vert.distance) {
                mark.erase(hash);
                mark.insert(make_pair(hash, vert.GiveWay()));
                q.push(Vertex(hash, vert.GiveWay() + "R", vert.GiveDist() + 1));
            }
        }
    }
    if (a.dist == -1) {
        cout << -1;
    } else {
        cout << a.dist << "\n";
        cout << a.way;
    }
}

int main() {
    unsigned long long n = 0;
    for (int i = 0; i < 16; i++) {
        unsigned long long t = 0;
        cin >> t;
        n = (n << 4) + t;
    }
    bfs(n);
    /*if (t.pos == -1) {
        cout << -1;
    } else {
        cout << t.dist << "\n";
        cout << t.way;
    }*/

    return 0;
}
